/*
  Filename  : Diag.c
*/

/* Standard Library */
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
/* Kernel */
#include "Typedefs.h"
#include "Kernel.h"
#include "UserInput.h"
#include "UserInterface.h"
#include "Log.h"
/* Driver */
#include "Buzzer.h"
#include "Contactless.h"
#include "Crypto.h"
#include "Ethernet.h"
#include "File.h"
#include "GPRS.h"
#include "Keypad.h"
#include "LCD.h"
#include "LED.h"
#include "MagStripe.h"
#include "Modem.h"
#include "ModemPPP.h"
#include "Power.h"
#include "Printer.h"
#include "RTC.h"
#include "SerialPort.h"
#include "SmartCard.h"
/* Library */
#include "Library.h"
/* Diag */
#include "DiagRespCodes.h"
/* Header File */
#include "Diag.h"

/* Log Defines */
#ifdef LOG_SHORTNAME
#undef LOG_SHORTNAME
#endif
#define LOG_SHORTNAME						"DIAGDRV"

/* Private Variables */
File_t  hDiagFile_;
MagTrack_t hDiagMagTrack_;
DateTime_t dateOjb;

/* Private Function Prototypes */
UInt8_t DiagDrv_Buzzer( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_Crypto( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_File( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_LCD( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_LED( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_Contactless( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_Ethernet( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_GPRS( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_Keypad( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_MagStripe( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_Modem( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_SmartCard( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_SerialPort( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_Printer( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_Power( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_ModemPPP( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagDrv_RTC( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );

/* Private Functions */
UInt8_t DiagDrv_Buzzer( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    UInt16_t    sLength;

    if( 0 == strcmp( "BEEP", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        Buzzer_Beep( (UInt16_t)StringToUInt32( iParam, sLength ) );
        return DIAG_RC_SUCCESS;
    }
	return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagDrv_Crypto( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Char_t	*pszParam1;
    Char_t	*pszParam2;
    Char_t	*pszParam3;
    Char_t      *pszParam4;
    UInt8_t     baKey[16];
    UInt8_t     baBuffer[1024];
    UInt16_t    sValue;
    UInt16_t    sLength;
    
    if( 0 == strcmp( "GETRND", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sValue = (UInt16_t)StringToUInt32( iParam, sLength );
        Crypto_GetRandomNumber( sValue, baBuffer );
        HexArrayToString( baBuffer, sValue, oResult );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DESECB", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam2 );
        if(( 16 != sLength ) && ( 32 != sLength ))
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( pszParam2, sLength, baKey );
        sLength = StringToHexArray( pszParam3, strlen(pszParam3), baBuffer );
        CryptoDES_ECB( (pszParam1[0] - '0'), baKey, baBuffer, sLength, baBuffer );
        HexArrayToString( baBuffer, sLength, oResult );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DESCBC", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) || ( NULL == pszParam4 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam2 );
        if(( 16 != sLength ) && ( 32 != sLength ))
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( pszParam2, sLength, baKey );
        
        if( 16 != strlen( pszParam3 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( pszParam3, 16, baBuffer );
        sLength = StringToHexArray( pszParam4, strlen(pszParam4), &baBuffer[8] );
        CryptoDES_CBC( (pszParam1[0] - '0'), baKey, baBuffer, &baBuffer[8], sLength, &baBuffer[8] );
        HexArrayToString( &baBuffer[8], sLength, oResult );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DESPKCS5", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) || ( NULL == pszParam4 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam2 );
        if(( 16 != sLength ) && ( 32 != sLength ))
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( pszParam2, sLength, baKey );
        
        if( 16 != strlen( pszParam3 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( pszParam3, 16, baBuffer );
        sLength = StringToHexArray( pszParam4, strlen(pszParam4), &baBuffer[8] );
        sLength = CryptoDES_CBC_PKCS5( (pszParam1[0] - '0'), baKey, baBuffer, &baBuffer[8], sLength, &baBuffer[8] );
        HexArrayToString( &baBuffer[8], sLength, oResult );
        return DIAG_RC_SUCCESS;
    }
	return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagDrv_File( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t	isResult;
    Char_t	*pszParam1;
    Char_t	*pszParam2;
    UInt8_t     baBuffer[1024];
    UInt8_t	bValue;
    UInt16_t    sLength;
    UInt32_t    lValue;
    
    if( 0 == strcmp( "OPEN", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
	pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
		bValue = (UInt8_t)StringToUInt32( pszParam2, strlen( pszParam2 ) );
		isResult = File_Open( pszParam1, bValue, &hDiagFile_ );
		oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLOSE", iFuncName ) )
    {
        File_Close( hDiagFile_ );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETOFF", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = StringToUInt32( iParam, sLength );
		isResult = File_SetOffset( hDiagFile_, lValue );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "READ", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = StringToUInt32( iParam, sLength );
        isResult = File_Read( hDiagFile_, baBuffer, &lValue );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString( baBuffer, lValue, &oResult[2] );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "WRITE", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = StringToHexArray( iParam, sLength, baBuffer );
        isResult = File_Write( hDiagFile_, baBuffer, lValue );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "RDLINE", iFuncName ) )
    {
        isResult = File_ReadLine( hDiagFile_, 1024, &oResult[2] );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "WRLINE", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = File_WriteLine( hDiagFile_, iParam );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "EXIST", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = File_Exist( iParam );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETSIZE", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = File_GetSize( iParam );
        sprintf( oResult, "%lu", lValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLR", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = File_ClearContent( iParam );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "COPY", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = File_Copy( pszParam1, pszParam2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "REN", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = File_Rename( pszParam1, pszParam2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DEL", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = File_Delete( iParam );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "UNZIP", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = File_Unzip( pszParam1, pszParam2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
	return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagDrv_LCD( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t	isResult;
    Char_t	*pszParam1;
    Char_t	*pszParam2;
    Char_t	*pszParam3;
    Char_t	*pszParam4;
    Char_t	*pszParam5;
    UInt8_t     baBuffer[1024];
    UInt8_t	bValue;
    UInt16_t    sLength;
    UInt16_t    sValue;
    UInt16_t    sValue2;
    UInt16_t    sValue3;
    UInt16_t    sValue4;
    UInt32_t    lValue;
    
    if( 0 == strcmp( "SETBL", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        LCD_SetBacklight( iParam[0] - '0' );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETFNT", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = LCD_SetFont( iParam );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETFGC", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 8 != sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( iParam, sLength, baBuffer );
        lValue = GetUInt32( baBuffer );
        LCD_SetForegroundColor( lValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETBGC", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 8 != sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( iParam, sLength, baBuffer );
        lValue = GetUInt32( baBuffer );
        LCD_SetBackgroundColor( lValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLR", iFuncName ) )
    {
        LCD_ClearScreen();
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "PIX", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sValue = (UInt16_t)StringToUInt32( pszParam1, strlen(pszParam1) );
        sValue2 = (UInt16_t)StringToUInt32( pszParam2, strlen(pszParam2) );
        LCD_DrawPixel( sValue, sValue2, (pszParam3[0] - '0') );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "BOX", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        pszParam5 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) || \
            ( NULL == pszParam4 ) || ( NULL == pszParam5 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sValue = (UInt16_t)StringToUInt32( pszParam1, strlen(pszParam1) );
        sValue2 = (UInt16_t)StringToUInt32( pszParam2, strlen(pszParam2) );
        sValue3 = (UInt16_t)StringToUInt32( pszParam3, strlen(pszParam3) );
        sValue4 = (UInt16_t)StringToUInt32( pszParam4, strlen(pszParam4) );
        LCD_DrawBox( sValue, sValue2, sValue3, sValue4, (pszParam5[0] - '0') );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "TXT", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || \
            ( NULL == pszParam3 ) || ( NULL == pszParam4 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sValue = (UInt16_t)StringToUInt32( pszParam1, strlen(pszParam1) );
        sValue2 = (UInt16_t)StringToUInt32( pszParam2, strlen(pszParam2) );
        StringToHexArray( pszParam3, strlen(pszParam3), baBuffer );
        sValue3 = GetUInt16( baBuffer );
        LCD_DrawText( sValue, sValue2, sValue3, pszParam4 );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "BMP", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        pszParam5 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) || \
            ( NULL == pszParam4 ) || ( NULL == pszParam5 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sValue = (UInt16_t)StringToUInt32( pszParam1, strlen(pszParam1) );
        sValue2 = (UInt16_t)StringToUInt32( pszParam2, strlen(pszParam2) );
        sValue3 = (UInt16_t)StringToUInt32( pszParam3, strlen(pszParam3) );
        sValue4 = (UInt16_t)StringToUInt32( pszParam4, strlen(pszParam4) );
        StringToHexArray( pszParam5, strlen(pszParam5), baBuffer );
        LCD_DrawBitmap( sValue, sValue2, sValue3, sValue4, baBuffer );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "IMG", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sValue = (UInt16_t)StringToUInt32( pszParam1, strlen(pszParam1) );
        sValue2 = (UInt16_t)StringToUInt32( pszParam2, strlen(pszParam2) );
        LCD_DrawImage( sValue, sValue2, pszParam3 );
        return DIAG_RC_SUCCESS;
    }
	return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagDrv_LED( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Char_t		*pszParam1;
    Char_t		*pszParam2;

    if( 0 == strcmp( "SET", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        LED_Set( (pszParam1[0] - '0'), (pszParam2[0] - '0') );
        return DIAG_RC_SUCCESS;
    }
	return DIAG_RC_INVALID_CMD;    
}

UInt8_t DiagDrv_Contactless( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t      isResult;
    Char_t      *pszParam1;
    Char_t      *pszParam2;
    Char_t      *pszParam3;
    Char_t      *pszParam4;
    Char_t      szBuffer[12];
    Char_t      szBuffer2[2];
    UInt8_t     bValue1;
    UInt8_t     bValue2;
    UInt8_t     baBuffer[1024];
    UInt8_t     baBuffer2[1024];
    UInt16_t    sLength;
    UInt32_t    lValue;
    
    if( 0 == strcmp( "OPEN", iFuncName ) )
    {
        isResult = CL_Open( );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLOSE", iFuncName ) )
    {
        CL_Close();
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETID", iFuncName ) )
    {
        CL_GetReaderID( oResult );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETLED", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || \
            ( NULL == pszParam3 ) || ( NULL == pszParam4 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        CL_SetLED( (pszParam1[0] - '0'), (pszParam2[0] - '0'), \
                   (pszParam3[0] - '0'), (pszParam4[0] - '0') );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETRF", iFuncName ) )
    {
        CL_SetRFPower( iParam[0] - '0' );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "ACTA", iFuncName ) )
    {
        isResult = CL_ActiveIdleTypeA( baBuffer, &bValue1, baBuffer2, &bValue2 );
        HexArrayToString( baBuffer, bValue1, szBuffer );
        HexArrayToString( baBuffer2, 2, szBuffer2 );
        sLength = sprintf( oResult, "%c|%.12s|%.4s|%02x", isResult + '0', szBuffer, szBuffer2, bValue2 );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCLOGIN", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( pszParam1, sLength );
        
        if( 2 != strlen( pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( pszParam2, strlen(pszParam2), baBuffer );
        bValue2 = (UInt8_t)GetUInt16( baBuffer );
        
        sLength = strlen( pszParam3 );
        if( 12 != sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( pszParam3, sLength, baBuffer2 );
        isResult = CL_MiFareClassicLogin( bValue1, bValue2, baBuffer2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCREAD", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( iParam, sLength );
        isResult = CL_MiFareClassicRead( bValue1, baBuffer );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString( baBuffer, strlen( baBuffer ), &oResult[2] );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCWRITE", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( pszParam1, sLength );
        StringToHexArray( pszParam2, strlen( pszParam2 ), baBuffer );
        isResult = CL_MiFareClassicWrite( bValue1, baBuffer );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCINC", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( pszParam1, sLength );
        
        sLength = strlen( pszParam2 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = StringToUInt32( pszParam2, sLength );
        isResult = CL_MiFareClassicIncrement( bValue1, lValue );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCDEC", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( pszParam1, sLength );
        
        sLength = strlen( pszParam2 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = StringToUInt32( pszParam2, sLength );
        isResult = CL_MiFareClassicDecrement( bValue1, lValue );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCTRS", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( pszParam1, sLength );
        
        sLength = strlen( pszParam2 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue2 = (UInt8_t)StringToUInt32( pszParam2, sLength );
        isResult = CL_MiFareClassicTransfer( bValue1, bValue2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagDrv_Ethernet( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t      isResult;
    Bool_t      bResult;
    Char_t      *pszParam1;
    Char_t      *pszParam2;
    Char_t      *pszParam3;
    UInt8_t     sLength;
    UInt8_t     baBuffer[1024];
    UInt8_t     baBuffer2[1024];
    UInt8_t     baBuffer3[1024];
    UInt16_t    sValue;
    UInt16_t    sLength2;
    
    if( 0 == strcmp( "OPEN", iFuncName ) )
    {
        isResult = Eth_Open( );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLOSE", iFuncName ) )
    {
        Eth_Close(  );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "STATUS", iFuncName ) )
    {
        sValue = Eth_GetStatus( );
        sprintf( oResult, "%x", sValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETDHCP", iFuncName ) )
    {
        isResult = Eth_GetDHCP( &bResult );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        oResult[2] = bResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETDHCP", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = Eth_SetDHCP( iParam[0] - '0' );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETIP", iFuncName ) )
    {
        isResult = Eth_GetLocalIP( baBuffer, baBuffer2, baBuffer3 );
        sprintf( oResult, "%c|%s|%s|%s|", isResult + '0', baBuffer, baBuffer2, baBuffer3 );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETIP", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = Eth_SetStaticIP( pszParam1, pszParam2, pszParam3 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETDNS", iFuncName ) )
    {
        isResult = Eth_GetDNS( &oResult[2] );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETDNS", iFuncName ) )
    {
        isResult = Eth_SetDNS( iParam );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CONNECT", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = Eth_Connect( pszParam1, pszParam2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DISCONN", iFuncName ) )
    {
        Eth_Disconnect( );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SEND", iFuncName ) )
    {
        sLength = strlen( iParam );
        sLength2 = StringToHexArray( iParam, sLength, baBuffer );
        isResult = Eth_Send( baBuffer, sLength2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "RECV", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sValue = ( UInt16_t )StringToUInt32( iParam, sLength );
        isResult = Eth_Receive( baBuffer, &sValue );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString( baBuffer, sValue, &oResult[2] );
        return DIAG_RC_SUCCESS;
    }
    return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagDrv_GPRS( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t      isResult;
    Char_t      *pszParam1;
    Char_t      *pszParam2;
    Char_t      *pszParam3;
    UInt8_t     sLength;
    UInt8_t     bValue;
    UInt8_t     baBuffer[1024];
    UInt16_t    sValue;
    UInt16_t    sLength2;
    
    if( 0 == strcmp( "OPEN", iFuncName ) )
    {
        isResult = Gprs_Open( );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLOSE", iFuncName ) )
    {
        Gprs_Close( );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SELSIM", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue = ( UInt8_t )StringToUInt32( iParam, sLength );
        isResult = Gprs_SelectSimSlot( bValue );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "ATTACH", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = Gprs_Attach( pszParam1, pszParam2, pszParam3 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "STATUS", iFuncName ) )
    {
        sValue = Gprs_GetStatus( );
        sprintf( oResult, "%x", sValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETERR", iFuncName ) )
    {
        bValue = Gprs_GetError( );
        sprintf( oResult, "%x", bValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETSIG", iFuncName ) )
    {
        isResult = Gprs_GetSignalLevel( &bValue );
        sprintf( oResult, "%c|%d", isResult + '0', bValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETINFO", iFuncName ) )
    {
        isResult = Gprs_GetInfo( &oResult[2] );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CONNECT", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = Gprs_Connect( pszParam1, pszParam2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DISCONN", iFuncName ) )
    {
        Gprs_Disconnect( );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SEND", iFuncName ) )
    {
        sLength = strlen( iParam );
        sLength2 = StringToHexArray( iParam, sLength, baBuffer );
        isResult = Gprs_Send( baBuffer, sLength2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "RECV", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sValue = ( UInt16_t )StringToUInt32( iParam, sLength );
        isResult = Gprs_Receive( baBuffer, &sValue );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString( baBuffer, sValue, &oResult[2] );
        return DIAG_RC_SUCCESS;
    }
    return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagDrv_Keypad( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    UInt8_t     sLength;
    UInt8_t     bValue;
    
    if( 0 == strcmp( "INIT", iFuncName ) )
    {
        Keyp_Init( );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETBL", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        Keyp_SetBacklight( iParam[0] - '0' );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETKEY", iFuncName ) )
    {
        bValue = Keyp_GetKeyPressed( );
        sprintf( oResult, "%x", bValue );
        return DIAG_RC_SUCCESS;
    }
    return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagDrv_MagStripe( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t      isResult;
    char        temp[50];
    
    if( 0 == strcmp( "CARDPEND", iFuncName ) )
    {
        isResult = MagStripe_IsCardPending( );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETTRACK", iFuncName ) )
    {
        isResult = MagStripe_Get( &hDiagMagTrack_ );
/// TOTEST        
        LOG_DEBUG_MSG( hDiagMagTrack_.m_PAN );
        LOG_DEBUG_MSG( hDiagMagTrack_.m_Name );
        LOG_DEBUG_MSG( hDiagMagTrack_.m_ExpiryDate );
        LOG_DEBUG_MSG( hDiagMagTrack_.m_ServiceCode );
        LOG_DEBUG_MSG( hDiagMagTrack_.m_DiscData );
/// TOTEST        
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        
        // To get the name without extra spaces
        strcpy(temp, hDiagMagTrack_.m_Name);
        int i = strlen(temp)-1;
        while(temp[i] == ' ')
        {
            i--;
        }
        memset(&temp[i+1], '\0', 1);
       
        sprintf( &oResult[2], "%s,%s,%s,%s,%s", hDiagMagTrack_.m_PAN,         \
                                                temp                ,        \
                                                hDiagMagTrack_.m_ExpiryDate,  \
                                                hDiagMagTrack_.m_ServiceCode, \
                                                hDiagMagTrack_.m_DiscData );
        return DIAG_RC_SUCCESS;
    }
    LOG_EXCEPTION();
    return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagDrv_Modem( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t      isResult;
    Char_t      *pszParam1;
    Char_t      *pszParam2;
    UInt8_t     sLength;
    UInt8_t     baBuffer[1024];
    UInt8_t     bValue;
    UInt16_t    sValue;
    UInt16_t    sLength2;
    
    if( 0 == strcmp( "OPEN", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue = ( UInt8_t )StringToUInt32( pszParam1, sLength );

        sLength = strlen( pszParam2 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = Mdm_Open( bValue, ( pszParam2[0] - '0' ) );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLOSE", iFuncName ) )
    {
        Mdm_Close( );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "STATUS", iFuncName ) )
    {
        sValue = Mdm_GetStatus( );
        sprintf( oResult, "%x", sValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETERR", iFuncName ) )
    {
        bValue = Mdm_GetErrorCode( );
        sprintf( oResult, "%x", bValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DIAL", iFuncName ) )
    {
        isResult = Mdm_Dial( iParam );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SEND", iFuncName ) )
    {
        sLength = strlen( iParam );
        sLength2 = StringToHexArray( iParam, sLength, baBuffer );
        isResult = Mdm_Send( baBuffer, sLength2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "RECV", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sValue = ( UInt16_t )StringToUInt32( iParam, sLength );
        isResult = Mdm_Receive( baBuffer, &sValue );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString( baBuffer, sValue, &oResult[2] );
        return DIAG_RC_SUCCESS;
    }
    return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagDrv_SmartCard( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Char_t		*pszParam1;
    Char_t		*pszParam2;
    Bool_t		isResult;
    UInt8_t             rLength;
    UInt8_t             rATR[1024], ihexArray[1024], ohexArray[1024];
    UInt8_t             rCardType;
    UInt32_t            value32;
    UInt16_t            value16,temp;
    
    if( 0 == strcmp( "CONNECT", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        isResult = SC_Connect( StringToUInt32( pszParam1, strlen(pszParam1) ), StringToUInt32( pszParam2, strlen(pszParam2) ), &rCardType, rATR, &rLength );
        value32 = sprintf(oResult,"%d|%02x|", isResult, rCardType);
        HexArrayToString( rATR, rLength, &oResult[value32] );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CONNEMV", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        isResult = SC_ConnectEMV( StringToUInt32( pszParam1, strlen(pszParam1) ), StringToUInt32( pszParam2, strlen(pszParam2) ), &rCardType, rATR, &rLength );
        value32 = sprintf(oResult,"%d|%02x|", isResult, rCardType);
        HexArrayToString( rATR, rLength, &oResult[value32] );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DISCONN", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        if( ( NULL == pszParam1 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        SC_Disconnect( StringToUInt32( pszParam1, strlen(pszParam1) ) );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "STATUS", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        if( ( NULL == pszParam1 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        value16 = SC_GetStatus( StringToUInt32( pszParam1, strlen(pszParam1) ) );
        sprintf(oResult, "%04x", value16);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "EXAPDU", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        value16 = StringToHexArray( pszParam2, strlen(pszParam2), ihexArray );
        isResult = SC_ExchangeAPDU( StringToUInt32( pszParam1, strlen(pszParam1) ) ,ihexArray, value16, ohexArray, &temp);
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString( ohexArray, temp, &oResult[2] );
        return DIAG_RC_SUCCESS;
    }
	return DIAG_RC_INVALID_CMD;    
}

UInt8_t DiagDrv_SerialPort( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Char_t		*pszParam1;
    Char_t		*pszParam2;
    Char_t		*pszParam3;
    Char_t		*pszParam4;
    Char_t		*pszParam5;
    Bool_t		isResult;
    UInt8_t             temp[1024], ohexArray[1024];
    UInt16_t            value16, rLength;
    
    if( 0 == strcmp( "OPEN", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        pszParam5 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) || ( NULL == pszParam4 ) || ( NULL == pszParam5 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        isResult = RS232_Open( StringToUInt32( pszParam1, strlen(pszParam1) ), StringToUInt32( pszParam2, strlen(pszParam2) ), StringToUInt32( pszParam3, strlen(pszParam3) ), StringToUInt32( pszParam4, strlen(pszParam4) ), StringToUInt32( pszParam5, strlen(pszParam5) ) );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLOSE", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        if( ( NULL == pszParam1 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        RS232_Close( StringToUInt32( pszParam1, strlen(pszParam1) ) );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SEND", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        value16 = StringToHexArray( pszParam2, strlen(pszParam2), ohexArray );
        isResult = RS232_Send( StringToUInt32( pszParam1, strlen(pszParam1) ), ohexArray, value16);
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "RECEIVE", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        if( ( NULL == pszParam1 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        isResult = RS232_Receive( StringToUInt32( pszParam1, strlen(pszParam1) ), temp, &rLength);
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString( temp, rLength, &oResult[2]);
        return DIAG_RC_SUCCESS;
    }
	return DIAG_RC_INVALID_CMD;    
}

UInt8_t DiagDrv_RTC( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t		isResult;
    Char_t		*day, *month, *year, *hour, *minute, *second;
    UInt32_t            lValue;
    
    if( 0 == strcmp( "GET", iFuncName ) )
    {
        isResult = RTC_GetDateTime(&dateOjb);
        oResult[0] = isResult + '0';
        oResult[1] = '|';    
        sprintf(&oResult[2], "%i,%i,%i,%i,%i,%i", dateOjb.m_Day, dateOjb.m_Month, dateOjb.m_Year, dateOjb.m_Hour, dateOjb.m_Minute, dateOjb.m_Second);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SET", iFuncName ) )
    {
        day = strtok( iParam, "," );
        month = strtok( NULL, "," );
        year = strtok( NULL, "," );
        hour = strtok( NULL, "," );
        minute = strtok( NULL, "," );
        second = strtok( NULL, "," );
        
        if( ( NULL == day ) || ( NULL == month ) || ( NULL == year ) || ( NULL == hour ) || ( NULL == minute ) || ( NULL == second ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        dateOjb.m_Day = StringToUInt32(day, strlen(day));
        dateOjb.m_Month = StringToUInt32(month, strlen(month));
        dateOjb.m_Year = StringToUInt32(year, strlen(year));
        dateOjb.m_Hour = StringToUInt32(hour, strlen(hour));
        dateOjb.m_Minute = StringToUInt32(minute, strlen(minute));
        dateOjb.m_Second = StringToUInt32(second, strlen(second));
        isResult = RTC_SetDateTime(&dateOjb);
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DELAY", iFuncName ) )
    {
        lValue = StringToUInt32( iParam, strlen(iParam) );
        Kernel_Wait( lValue );
        return DIAG_RC_SUCCESS;
    }
    return DIAG_RC_INVALID_CMD;    
}

UInt8_t DiagDrv_Printer( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t		isResult;
    UInt8_t             status;
    UInt8_t             ohexArray[1024];
    Char_t		*pszParam1, *pszParam2, *pszParam3, *pszParam4;
    
    if( 0 == strcmp( "STATUS", iFuncName ) )
    {
        status = Printer_GetStatus();
        sprintf(oResult, "%02x", status);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETFNT", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        if( ( NULL == pszParam1 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        isResult = Printer_SetFont(pszParam1);
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETDARK", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        if( ( NULL == pszParam1 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        isResult = Printer_SetDarkness(StringToUInt32(pszParam1, strlen(pszParam1)));
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "PRINT", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        if( ( NULL == pszParam1 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        status = Printer_QuickPrint(pszParam1);
        sprintf(oResult, "%02x", status);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "FORMFD", iFuncName ) )
    {
        Printer_FormFeed();
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "LINEFD", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        if( ( NULL == pszParam1 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        Printer_LineFeed(StringToUInt32(pszParam1, strlen(pszParam1)));
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLR", iFuncName ) )
    {
        Printer_BufferClear();
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "FLUSH", iFuncName ) )
    {
        status = Printer_BufferFlush();
        sprintf(oResult, "%02x", status);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "PUTSTR", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) || strlen(pszParam2) != 4)
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray(pszParam2, strlen(pszParam2), ohexArray);
        status = Printer_BufferPutString(StringToUInt32(pszParam1, strlen(pszParam1)), GetUInt16(ohexArray), pszParam3);
        sprintf(oResult, "%02x", status);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "PUTBMP", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) || ( NULL == pszParam4 ))
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray(pszParam4, strlen(pszParam4), ohexArray);
        status = Printer_BufferPutBitmap(StringToUInt32(pszParam1, strlen(pszParam1)), StringToUInt32(pszParam2, strlen(pszParam2)), StringToUInt32(pszParam3, strlen(pszParam3)), ohexArray);
        sprintf(oResult, "%02x", status);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "PUTRECT", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) || ( NULL == pszParam4 ))
        {
            return DIAG_RC_INVALID_PARAM;
        }
        status = Printer_BufferPutRectangle(StringToUInt32(pszParam1, strlen(pszParam1)), StringToUInt32(pszParam2, strlen(pszParam2)), StringToUInt32(pszParam3, strlen(pszParam3)), StringToUInt32(pszParam4, strlen(pszParam4)));
        sprintf(oResult, "%02x", status);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "PUTBAR13", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        status = Printer_BufferPutBarcodeEAN13(StringToUInt32(pszParam1, strlen(pszParam1)), pszParam2);
        sprintf(oResult, "%02x", status);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "PUTBAR128", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        status = Printer_BufferPutBarcodeCode128(StringToUInt32(pszParam1, strlen(pszParam1)), pszParam2);
        sprintf(oResult, "%02x", status);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "PUTQR", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        status = Printer_BufferPutQRCode(StringToUInt32(pszParam1, strlen(pszParam1)), pszParam2);
        sprintf(oResult, "%02x", status);
        return DIAG_RC_SUCCESS;
    }
	return DIAG_RC_INVALID_CMD;    
}


UInt8_t DiagDrv_Power( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t      isResult;
    UInt8_t     rValue;
    
    if( 0 == strcmp( "GETSRC", iFuncName ) )
    {
        isResult = Power_GetSource(&rValue);
        sprintf(oResult, "%d|%d", isResult,rValue);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "STATUS", iFuncName ) )
    {
        isResult = Power_GetBatteryStatus(&rValue);
        sprintf(oResult, "%d|%02x", isResult,rValue);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETCAP", iFuncName ) )
    {
        isResult = Power_GetBatteryCapacity(&rValue);
        sprintf(oResult, "%d|%d", isResult,rValue);
        return DIAG_RC_SUCCESS;
    }
	return DIAG_RC_INVALID_CMD;    
}

UInt8_t DiagDrv_ModemPPP( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t      isResult;
    UInt16_t    rValue16;
    UInt8_t     rValue8;
    Char_t      *pszParam1, *pszParam2, *pszParam3, *pszParam4, *localIP;
    UInt8_t     oArray[1024];
    
    if( 0 == strcmp( "OPEN", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        if( ( NULL == pszParam1 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = PPP_Open(StringToUInt32(pszParam1, strlen(pszParam1)));
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLOSE", iFuncName ) )
    {
        PPP_Close();
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "STATUS", iFuncName ) )
    {
        rValue16 = PPP_GetStatus();
        sprintf(oResult, "%04x", rValue16);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETERR", iFuncName ) )
    {
        rValue8 = PPP_GetError();
        sprintf(oResult, "%02x", rValue8);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETINFO", iFuncName ) )
    {
        isResult = PPP_GetInfo(localIP);
        sprintf(oResult, "%d|%s", isResult, localIP);
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DIAL", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) || 
            ( NULL == pszParam4 ))
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = PPP_Dial(pszParam1, StringToUInt32(pszParam2, strlen(pszParam2)), pszParam3, pszParam4);
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CONNECT", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = PPP_Connect(pszParam1, pszParam2);
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DISCONN", iFuncName ) )
    {
        PPP_Disconnect();
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SEND", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        if( ( NULL == pszParam1 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( pszParam1, strlen(pszParam1), oArray );
        isResult = PPP_Send(oArray,strlen(oArray));
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "RECV", iFuncName ) )
    {
        UInt16_t length;
        isResult = PPP_Receive(oArray, &length);
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString(oArray, length, &oResult[2] );
        return DIAG_RC_SUCCESS;
    }
	return DIAG_RC_INVALID_CMD;    
}

/* Public Functions */
UInt8_t DiagDrv_Execute( Char_t iCommand[], Char_t iParam[], Char_t oResult[] )
{
    Char_t	szFuncName[17]; /* up to 16 chars */
    UInt8_t     bStatus;

    memset( szFuncName, 0, sizeof(szFuncName) );
    memcpy( szFuncName, &iCommand[4], (strlen(iCommand) - 4) );    
    
    /* Compare module name */
    if( 0 == memcmp( "BUZ", &iCommand[1], 3 ) ) /* Buzzer */
    {
        bStatus = DiagDrv_Buzzer( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "CRY", &iCommand[1], 3 ) ) /* Crypto */
    {
        bStatus = DiagDrv_Crypto( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "FIL", &iCommand[1], 3 ) ) /* File */
    {
        bStatus = DiagDrv_File( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "LCD", &iCommand[1], 3 ) ) /* LCD */
    {
        bStatus = DiagDrv_LCD( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "LED", &iCommand[1], 3 ) ) /* LED */
    {
        bStatus = DiagDrv_LED( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "CLR", &iCommand[1], 3 ) ) /* Contactless */
    {
        bStatus = DiagDrv_Contactless( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "ETH", &iCommand[1], 3 ) ) /* Ethernet */
    {
        bStatus = DiagDrv_Ethernet( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "GPR", &iCommand[1], 3 ) ) /* GPRS */
    {
        bStatus = DiagDrv_GPRS( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "KEY", &iCommand[1], 3 ) ) /* Keypad */
    {
        bStatus = DiagDrv_Keypad( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "MAG", &iCommand[1], 3 ) ) /* MagStripe */
    {
        bStatus = DiagDrv_MagStripe( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "MDM", &iCommand[1], 3 ) ) /* Modem */
    {
        bStatus = DiagDrv_Modem( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "SCR", &iCommand[1], 3 ) ) /* Smart Card */
    {
        bStatus = DiagDrv_SmartCard( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "SER", &iCommand[1], 3 ) ) /* Serial Port */
    {
        bStatus = DiagDrv_SerialPort( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "RTC", &iCommand[1], 3 ) ) /* Serial Port */
    {
        bStatus = DiagDrv_RTC( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "PRT", &iCommand[1], 3 ) ) /* Printer */
    {
        bStatus = DiagDrv_Printer( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "POW", &iCommand[1], 3 ) ) /* Power */
    {
        bStatus = DiagDrv_Power( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "PPP", &iCommand[1], 3 ) ) /* Modem PPP */
    {
        bStatus = DiagDrv_ModemPPP( szFuncName, iParam, oResult );
    }
    else
    {
        LOG_EXCEPTION();
        bStatus = DIAG_RC_INVALID_CMD;
    }
    return bStatus;
}

/* EOF */
