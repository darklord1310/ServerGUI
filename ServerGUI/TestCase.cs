﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerGUI
{
    public class TestCase
    {
        private string tcNo;
        private string desc;
        private List<SequenceNum> seqNo = new List<SequenceNum>();

        public TestCase() { }

        public TestCase(string tcNo, string desc)
        {
            setTcNo(tcNo);
            setDesc(desc);
        }

        public string getTcNo() { return tcNo; }
        public string getDesc() { return desc; }
        public List<SequenceNum> getSeqNum() { return seqNo; }

        public void setTcNo(string tc) { tcNo = tc; }
        public void setDesc(string d) { desc = d; }
        public void setSeqNum(List<SequenceNum> sq) { seqNo = sq; }

        public void addSeqNum(SequenceNum sqNum)
        {
            seqNo.Add(sqNum);
        }
    }
}
