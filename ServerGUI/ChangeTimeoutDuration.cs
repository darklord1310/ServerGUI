﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerGUI
{
    public partial class ChangeTimeoutDuration : Form
    {
        private int timeout;

        public ChangeTimeoutDuration()
        {
            InitializeComponent();
        }

        public ChangeTimeoutDuration(int duration)
        {
            InitializeComponent();
            this.TopMost = true;
            textBox1.Text = duration.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                timeout = Convert.ToInt32(textBox1.Text);
                if (timeout == 0)
                    timeout = -1;
                this.Hide();
            }
            catch(FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public int getTimeoutDuration()
        {
            return timeout;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timeout = -1;
            this.Hide();
        }
    }
}
