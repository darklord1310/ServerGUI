﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace ServerGUI
{
    public class SQLDatabase
    {
        // Create the sqlite database according to the DBName
        public void createDatabase(string DBName)
        {
            DBName = DBName + ".sqlite";
            SQLiteConnection.CreateFile(DBName);
        }


        // Create the connection for specific database
        public SQLiteConnection createDBConnection(string DBName, SQLiteConnection m_dbConnection)
        {
            DBName = DBName + ".sqlite";
            m_dbConnection = new SQLiteConnection("Data Source=" + DBName + ";Version=3;" + "PRAGMA AUTO_VACUUM = true");
            return m_dbConnection;
        }

        public int addData(string command, string expected, string actual, int result, SQLiteCommand com)
        {
            com.Parameters.AddWithValue("@command", command);
            com.Parameters.AddWithValue("@expected", expected);
            com.Parameters.AddWithValue("@actual", actual);
            com.Parameters.AddWithValue("@result", result);

            return com.ExecuteNonQuery();
        }

        // Open the connection for specific database
        public void connectToDB(SQLiteConnection connection)
        {
            connection.Open();
        }

        // Create the database (One time only)
        public void createTestCasesTable(SQLiteConnection connection)
        {
            string test = "create table if not exists TestCases (command text, expected text, actual text, result int)";
            SQLiteCommand command = new SQLiteCommand(test, connection);
            command.ExecuteNonQuery();
        }


        public bool isRecordExists(SQLiteConnection connection, string itemToSearch)
        {
            string temp = "'" + itemToSearch + "'";
            string sql = "SELECT count(*) from TestCases where command like " + temp;
            SQLiteCommand sqlCommand = new SQLiteCommand(sql, connection);

            int count = Convert.ToInt32(sqlCommand.ExecuteScalar());
            if (count == 0)
                return false;
            else
                return true;
        }

        // This function will update the specific row of data based on the 'command'
        public void updateSpecificDataInDB(SQLiteCommand command, string itemToSearch, string expected, string actual, int result)
        {
            command.Parameters.AddWithValue("@itemToSearch", itemToSearch);
            command.Parameters.AddWithValue("@expected", expected);
            command.Parameters.AddWithValue("@actual", actual);
            command.Parameters.AddWithValue("@result", result);
  
            command.ExecuteNonQuery();
        }

        public void closeConnection(SQLiteConnection m_dbConnection)
        {
            m_dbConnection.Close();
        }

    }
}
