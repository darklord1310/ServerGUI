﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerGUI
{
    public class TestMenu
    {
        private string category;
        private string module;
        private List<TestCase> tc = new List<TestCase>();

        public TestMenu() { }

        public TestMenu(string cg, string mod)
        {
            setCategory(cg);
            setModule(mod);
        }

        public string getCategory() { return category; }
        public string getModule() { return module; }
        public List<TestCase> getTestCase() { return tc; }

        public void setCategory(string category) { this.category = category; }
        public void setModule(string mod) { module = mod; }
        public void setTestCase(List<TestCase> tc) { this.tc = tc; }

        public void addTestCase(TestCase tc)
        {
            this.tc.Add(tc);
        }
    }
}
