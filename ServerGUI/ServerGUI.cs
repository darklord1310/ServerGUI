﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
using System.Diagnostics;
using System.Data.SQLite;
using MyCustomControl;

namespace ServerGUI
{
    public partial class TestServerGUI : Form
    {
        public List<TestMenu> tm = new List<TestMenu>();
        public List<TestCase> tc = new List<TestCase>();
        public SequenceNum sn;

        //constant
        const bool userVerifiedTest = true;     /* true = manual tests, false = auto tests */
        const bool failTest = false;

        //int to hold the total number of checkboxes in treeview
        int totalCheck = 0;

        //int to hold the total number of fail and pass tests
        int totalPassTest = 0;
        int totalFailTest = 0;

        //int to store the total manual and automatic test case
        int totalManualTest = 0;
        int totalAutoTest = 0;
        //int currentNoOfManualTest = 0;
        //int currentNoOfAutoTest = 0;

        //int to store the current testing sequence index number
        int seqIndex;

        //bool to indicated whether the test started
        bool isTestStarted = false;

        // Status to indicate the connection of the client, initialize as false initially
        bool clientConnected = false;

        //string to hold the user file dir, initialize as empty
        String singleXMLDir = string.Empty;
        String folderXMLDir = string.Empty;

        string logFilePath = string.Empty;
        StreamWriter logWriter;
        string dataReceive = string.Empty;
        string IpAddress;
        bool terminalBusy = false;
        string settingForTest = Constants.defaultTestSetting;          // The test setting indicator
        int portNumber = Constants.defaultPort;
        string serverStatus = Constants.defaultServerStatus;
        Socket listener = null;
        Socket handler = null;
        bool isXmlFolderSelected = false;
        AsyncCallback pfnWorkerCallBack;
        SQLDatabase db;
        SQLiteConnection connection;
        string filename = "database";               // hardcoded name for database
        diagResponse cmd;
        databaseData returnData;
        int timeout = Constants.defaultTimeout;
        FileSystemWatcher watcher;
        static bool watcherResponse = false;        // watcherResponse is the variable which will keep track the msgBox status
        System.Threading.Timer timer;
        enum Images { UNDETERMINED, PASS, FAIL, START, STOP };

        // all the neccessary constants are declared here
        static class Constants
        {
            public const int defaultPort = 25000;
            public const string defaultConnectStatus = "Disconnected";
            public const string defaultTestSetting = "Halt";
            public const string defaultServerStatus = "Offline";
            public const int defaultTimeout = 5000;
        }

        public class StateObject
        {
            // Client  socket.
            public Socket workSocket = null;
            // Size of receive buffer.
            public const int BufferSize = 10025;
            // Receive buffer.
            public byte[] buffer = new byte[BufferSize];
            // Received data string.
            public string data;
        }

        struct diagResponse
        {
            public string diagCommand;
            public string returnData;
            public int responceCode;
        };

        struct databaseData
        {
            public string diagCommand;
            public string testCase;
            public string seqNumber;
            public string actual;
            public int result;
        };

        public TestServerGUI()
        {
            InitializeComponent();
            cmd = new diagResponse();
            db = new SQLDatabase();
            watcher = new FileSystemWatcher();
            returnData = new databaseData();
            myTreeView.CheckBoxes = true;
            startTestButton.Enabled = false;
            StartServerButton.Enabled = true;
            StopServerButton.Enabled = false;
            keepTrackHostClient.RunWorkerAsync();
        }

        private void continueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Create a new instance of the OpenFileDialog
            OpenFileDialog dialog = new OpenFileDialog();

            //Set the file filter
            dialog.Filter = "xml files (*.xml)|*.xml";

            //Set Initial Directory
            dialog.InitialDirectory = Directory.GetCurrentDirectory() + "/TestCaseXML";
            dialog.Title = "Select a xml file";

            //Present to the user. 
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                isXmlFolderSelected = false;
                singleXMLDir = dialog.FileName;
                watchFile();                // monitor any changes in xml files
                tm.Clear();
                tc.Clear();
                buildTreeViewFromXML();
                loadDatabaseIntoTree();
            }
            if (singleXMLDir == String.Empty)
                return;//user didn't select a file
        }


        private void OnChanged(object source, FileSystemEventArgs e)
        {
            // Specify what is done when a file is changed, created, or deleted.
            watcherResponse = true;     // raise the files modified flag
        }

        private void watchFile()
        {
            watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);
            
            if(isXmlFolderSelected == false)
            {
                watcher.Path = Path.GetDirectoryName(singleXMLDir);
                watcher.Filter = Path.GetFileName(singleXMLDir);
            }
            else
            {
                watcher.Path = folderXMLDir;
            }
              
            watcher.EnableRaisingEvents = true;
        }

        private void openAllXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                isXmlFolderSelected = true;
                folderXMLDir = folderDialog.SelectedPath;
                watchFile();                // monitor any changes in xml files
                tm.Clear();
                tc.Clear();
                buildTreeViewFromXML();
                loadDatabaseIntoTree();
            }

            if (folderXMLDir == String.Empty)
                return;//user didn't select a file
        }

        /*
         * Populate the tree from the xml file
         */
        private void buildTreeViewFromXML()
        {
            XmlDocument xmlDoc = new XmlDocument();
            string[] files;
            int count = 0;

            if (this.InvokeRequired)
            {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    myTreeView.BeginUpdate();
                    myTreeView.Nodes.Clear();

                    if (isXmlFolderSelected)
                    {
                        files = Directory.GetFiles(folderXMLDir, "*.xml");
                    }
                    else
                    {
                        files = new string[1] { singleXMLDir };
                    }

                    foreach (string file in files)
                    {
                        if (!nodeExists(myTreeView, "TestMenu", 0))
                        {
                            myTreeView.Nodes.Add(new TreeNode("TestMenu") { Name = "TestMenu" });
                        }

                        try
                        {
                            xmlDoc.Load(file);
                        }
                        catch(Exception ex)
                        {
                            showMsgBox(Path.GetFileName(file) + " format is incorrect!\n\n" + "Error: " + ex.Message, MessageBoxIcon.Error);
                        }
                    

                        foreach (XmlNode xNode in xmlDoc.SelectNodes("TestMenu"))
                        {
                            try
                            {
                                // to avoid duplicate menu in treeview
                                if (!nodeExists(myTreeView, xNode["Category"].InnerText, 1))
                                {
                                    myTreeView.Nodes["TestMenu"].Nodes.Add(new TreeNode(xNode["Category"].InnerText) { Name = xNode["Category"].InnerText });
                                }

                                if (!nodeExists(myTreeView, xNode["Module"].InnerText, 2))
                                {
                                    myTreeView.Nodes["TestMenu"].Nodes[xNode["Category"].InnerText].Nodes.Add(new TreeNode(xNode["Module"].InnerText) { Name = xNode["Module"].InnerText });
                                }

                                tm.Add(new TestMenu(xNode["Category"].InnerText, xNode["Module"].InnerText));

                                XmlNodeList testCaseList = xNode.ChildNodes;
                                for (int i = 2; i < testCaseList.Count; i++)
                                {
                                    XmlNode tcNode = xNode.ChildNodes[i];
                                    myTreeView.Nodes["TestMenu"].Nodes[xNode["Category"].InnerText].Nodes[xNode["Module"].InnerText].Nodes.Add(new TreeNode(tcNode.Attributes["tc"].InnerText) { Name = tcNode.Attributes["tc"].InnerText });
                                    tc.Add(new TestCase(tcNode.Attributes["tc"].InnerText, tcNode.SelectSingleNode("Desc").InnerText));
                                    //MessageBox.Show(tc.Count.ToString());

                                    XmlNodeList seqNumList = tcNode.ChildNodes;
                                    for (int j = 1; j < seqNumList.Count; j++)
                                    {
                                        XmlNode sqNode = seqNumList[j];

                                        sn = new SequenceNum(sqNode.Attributes["sn"].InnerText,
                                                             sqNode.SelectSingleNode("Desc").InnerText,
                                                             sqNode.SelectSingleNode("DiagCmd").InnerText,
                                                             sqNode.SelectSingleNode("Param").InnerText,
                                                             sqNode.SelectSingleNode("Expect").InnerText);

                                        tc[i - 2].addSeqNum(sn);
                                    }

                                    tm[tm.Count - 1].addTestCase(tc[i - 2]);
                                    //displayTestCase(tc[i - 2]);
                                }

                                //displayTestMenu(tm[tm.Count - 1]);
                                tc.RemoveRange(0, tc.Count);
                                count++;
                            }
                            catch (Exception)
                            {
                                showMsgBox("Some " + tm[tm.Count - 1].getModule() + " XML elements is incorrect! It will not be loaded.", MessageBoxIcon.Error);
                                myTreeView.Nodes.Remove(myTreeView.Nodes["TestMenu"].Nodes[xNode["Category"].InnerText].Nodes[tm[tm.Count - 1].getModule()]);
                                tm.RemoveAt(count);
                            }
                        }
                    }

                    myTreeView.EndUpdate();
                    //checkAllSelectionTests(myTreeView.Nodes, autochkBox.Checked, false);
                    //checkAllSelectionTests(myTreeView.Nodes, manualchkBox.Checked, true);

                    //Update the total of manual and auto tests
                    if (myTreeView.Nodes.Count > 0)
                    {
                        totalAutoTest = numOfAllSelectionTestCheck(myTreeView.Nodes, !userVerifiedTest);
                        totalManualTest = numOfAllSelectionTestCheck(myTreeView.Nodes, userVerifiedTest);
                    }
                });
            }
            else
            {
                myTreeView.BeginUpdate();
                myTreeView.Nodes.Clear();

                if (isXmlFolderSelected)
                {
                    files = Directory.GetFiles(folderXMLDir, "*.xml");
                }
                else
                {
                    files = new string[1] { singleXMLDir };
                }

                foreach (string file in files)
                {
                    if (!nodeExists(myTreeView, "TestMenu", 0))
                    {
                        myTreeView.Nodes.Add(new TreeNode("TestMenu") { Name = "TestMenu" });
                    }

                    try
                    {
                        xmlDoc.Load(file);
                    }
                    catch(Exception ex)
                    {
                        showMsgBox(Path.GetFileName(file) + " format is incorrect!\n\n" + "Error: " + ex.Message, MessageBoxIcon.Error);
                    }
                    
                    foreach (XmlNode xNode in xmlDoc.SelectNodes("TestMenu"))
                    {
                        try
                        {
                            // to avoid duplicate menu in treeview
                            if (!nodeExists(myTreeView, xNode["Category"].InnerText, 1))
                            {
                                myTreeView.Nodes["TestMenu"].Nodes.Add(new TreeNode(xNode["Category"].InnerText) { Name = xNode["Category"].InnerText });
                            }

                            if (!nodeExists(myTreeView, xNode["Module"].InnerText, 2))
                            {
                                myTreeView.Nodes["TestMenu"].Nodes[xNode["Category"].InnerText].Nodes.Add(new TreeNode(xNode["Module"].InnerText) { Name = xNode["Module"].InnerText });
                            }

                            tm.Add(new TestMenu(xNode["Category"].InnerText, xNode["Module"].InnerText));

                            XmlNodeList testCaseList = xNode.ChildNodes;
                            for (int i = 2; i < testCaseList.Count; i++)
                            {
                                XmlNode tcNode = xNode.ChildNodes[i];
                                myTreeView.Nodes["TestMenu"].Nodes[xNode["Category"].InnerText].Nodes[xNode["Module"].InnerText].Nodes.Add(new TreeNode(tcNode.Attributes["tc"].InnerText) { Name = tcNode.Attributes["tc"].InnerText });
                                tc.Add(new TestCase(tcNode.Attributes["tc"].InnerText, tcNode.SelectSingleNode("Desc").InnerText));
                                //MessageBox.Show(tc.Count.ToString());

                                XmlNodeList seqNumList = tcNode.ChildNodes;
                                for (int j = 1; j < seqNumList.Count; j++)
                                {
                                    XmlNode sqNode = seqNumList[j];

                                    sn = new SequenceNum(sqNode.Attributes["sn"].InnerText,
                                                            sqNode.SelectSingleNode("Desc").InnerText,
                                                            sqNode.SelectSingleNode("DiagCmd").InnerText,
                                                            sqNode.SelectSingleNode("Param").InnerText,
                                                            sqNode.SelectSingleNode("Expect").InnerText);

                                    tc[i - 2].addSeqNum(sn);
                                }

                                tm[tm.Count - 1].addTestCase(tc[i - 2]);
                                //displayTestCase(tc[i - 2]);
                            }

                            //displayTestMenu(tm[tm.Count - 1]);
                            tc.RemoveRange(0, tc.Count);
                            count++;
                        }
                        catch (Exception)
                        {
                            showMsgBox("Some " + tm[tm.Count - 1].getModule() + " XML elements is incorrect! It will not be loaded.", MessageBoxIcon.Error);
                            myTreeView.Nodes.Remove(myTreeView.Nodes["TestMenu"].Nodes[xNode["Category"].InnerText].Nodes[tm[tm.Count - 1].getModule()]);
                            tm.RemoveAt(count);
                        }
                    }
                }

                myTreeView.EndUpdate();
                //checkAllSelectionTests(myTreeView.Nodes, autochkBox.Checked, false);
                //checkAllSelectionTests(myTreeView.Nodes, manualchkBox.Checked, true);

                //Update the total of manual and auto tests
                if (myTreeView.Nodes.Count > 0)
                {
                    totalAutoTest = numOfAllSelectionTestCheck(myTreeView.Nodes, !userVerifiedTest);
                    totalManualTest = numOfAllSelectionTestCheck(myTreeView.Nodes, userVerifiedTest);
                }
            }
        }

        /*
         * To check if certain node exist
         */
        private bool nodeExists(TreeView tv, string key, int level)
        {
            try
            {
                TreeNode[] nodeArr = tv.Nodes.Find(key, true);
                for (int i = 0; i < nodeArr.Length; i++)
                {
                    if (String.Equals(nodeArr[i].Text, key) && nodeArr[i].Level == level)
                        return true;
                }
            }
            catch { }

            return false;
        }

        /*
         * For debugging purposes
         * To verify the value extract from xml
         */
        private void displayTestMenu(TestMenu tm)
        {
            List<TestCase> tCase = tm.getTestCase();

            for (int i = 0; i < tCase.Count; i++)
            {
                txtBoxDes.Text += "Test Case: " + tCase[i].getTcNo() + Environment.NewLine;
                displayTestCase(tCase[i]);
            }
        }

        /*
         * For debugging purposes
         * To verify the value extract from xml and display in the description text box
         */
        private void displayTestCase(TestCase tCase)
        {
            List<SequenceNum> sq = tCase.getSeqNum();

            for (int i = 0; i < sq.Count; i++)
            {
                txtBoxDes.Text += "sn: " + sq[i].getSeqNo() + Environment.NewLine;
            }
        }

         /*
          * Get the host network info, such as IP address and Port number
          */
         private void getServerInfo()
         {
            IPAddress[] ipAddressArray = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress ipAddress in ipAddressArray)
            {
                if (ipAddress.AddressFamily == AddressFamily.InterNetwork)
                {
                    IpAddress = ipAddress.ToString();
                }
            }

            //Some delays to avoid GetHostAddresses fail
            Thread.Sleep(100);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            while(true)
            {
                if (keepTrackHostClient.CancellationPending == false)
                    updateHostAndClientInfo();    //this function will keep the host and client info up to date once the application is started
                else
                    break;
            }
        }

        private void updateHostAndClientInfo()
        {
            getServerInfo();
            testSettingStatuslbl.Text = "Test Setting : " + settingForTest;
            serverStatuslbl.Text = "Server Status : " + serverStatus;
            IPStatuslbl.Text = "Host IP : " + IpAddress;
            PortStatuslbl.Text = "Port : " + portNumber;             
            keepTrackHostClient.ReportProgress(0);
        }

        /*
         * Updates all child tree nodes recursively.
         */
        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            expandCollapseNode(treeNode, nodeChecked);

            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    // If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                    this.CheckAllChildNodes(node, nodeChecked);
                }
            }
        }

        /*
         * Expand or collapse the node depend on the node check state
         */
        private void expandCollapseNode(TreeNode treeNode, bool nodeChecked)
        {
            if (treeNode.Checked)
                treeNode.Expand();
            else
                treeNode.Collapse();
        }
          
        /*
         * NOTE   This code can be added to the BeforeCheck event handler instead of the AfterCheck event.
         * After a tree node's Checked property is changed, all its child nodes are updated to the same value.
         */
        private void myTreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (!isTestStarted)
            {
                // The code only executes if the user caused the checked state to change.
                if (e.Action != TreeViewAction.Unknown)
                {
                    updateParentNodeCheck(e.Node);

                    if (e.Node.Nodes.Count > 0)
                    {
                        /* Calls the CheckAllChildNodes method, passing in the current 
                        Checked value of the TreeNode whose checked state changed. */
                        this.CheckAllChildNodes(e.Node, e.Node.Checked);
                    }
                }

                //if(isSelectionTest(e.Node, userVerifiedTest))
                //{
                    ////updateTotalSelectionTest(userVerifiedTest, e.Node.Checked);
                    ////lblPassTest.Text = currentNoOfManualTest.ToString();  /* For debugging purposes */

                //    if (currentNoOfManualTest == 0)
                //        manualchkBox.CheckState = CheckState.Unchecked;
                //}
                //else if (isSelectionTest(e.Node, !userVerifiedTest))
                //{
                    ////updateTotalSelectionTest(!userVerifiedTest, e.Node.Checked);
                    ////lblFailTest.Text = currentNoOfAutoTest.ToString();    /* For debugging purposes */

                //    if (currentNoOfAutoTest == 0)
                //        autochkBox.CheckState = CheckState.Unchecked;
                //}
            }

            totalTickCheckedBox(myTreeView.Nodes["TestMenu"]);
            lblTotalTest.Text = totalCheck.ToString();
            totalCheck = 0;
        }

         /*
          * Pass in the parent node here and check if the parent node has any childnode selected
          */
        private bool checkIsAnyChildNodeSelected(TreeNode node)
        {
            TreeNode travelNode = node.FirstNode;
            while(true)
            {
                if (travelNode.Checked == true)
                {
                    return true;
                }

                if (travelNode == node.LastNode)
                    break;
                else
                    travelNode = travelNode.NextNode;
            }
            return false;
        }

        /*
         * Update the parent node check state
         */
        private void updateParentNodeCheck(TreeNode node)
        {
            if(node.Parent != null)
            {
                if (checkIsAnyChildNodeSelected(node.Parent) == true)
                {
                    node.Parent.Checked = true;
                }
                else
                    node.Parent.Checked = false;
                
                updateParentNodeCheck(node.Parent);
            }
        }

        /*
         * To update the total test in the Summary groupbox
         */
        private void totalTickCheckedBox(TreeNode tNode)
        {
            foreach(TreeNode child in tNode.Nodes)
            {
                if (child.Checked)
                {
                    if (child.Level > 2)
                    {
                        totalCheck++;
                    }
                }

                if (child.Nodes.Count > 0)
                    totalTickCheckedBox(child);
            }
        }

        private void myTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            List<TestCase> testCase;
            List<SequenceNum> sqNum;
            TreeNode selNode = e.Node;

            txtBoxDes.Clear();
            tcDataGridView.Rows.Clear();

            if (selNode.Level == 3)
            {
                testCase = getNodeTestCase(selNode);
                txtBoxDes.Text = testCase[selNode.Index].getDesc();
                sqNum = getNodeSeqNum(selNode);

                for (int i = 0; i < sqNum.Count; i++)
                {
                    tcDataGridView.Rows.Add();
                    tcDataGridView.Rows[i].Cells["SeqNo"].Value = sqNum[i].getSeqNo();
                    tcDataGridView.Rows[i].Cells["DiagnosticCmd"].Value = sqNum[i].getDiagCmd();
                    tcDataGridView.Rows[i].Cells["Description"].Value = sqNum[i].getDesc();
                    tcDataGridView.Rows[i].Cells["Parameter"].Value = sqNum[i].getPara();
                    tcDataGridView.Rows[i].Cells["ExpOutcome"].Value = sqNum[i].getExpected();
                    tcDataGridView.Rows[i].Cells["Response"].Value = sqNum[i].getActual();
                    
                    if(sqNum[i].getResult() != -1)
                    {
                        if (sqNum[i].getResult() == 1)
                        {
                            tcDataGridView.Rows[i].Cells["Status"].Value = "PASS";
                            tcDataGridView.Rows[i].Cells["Status"].Style.BackColor = Color.LimeGreen;
                            tcDataGridView.Rows[i].Cells["Status"].Style.SelectionBackColor = Color.LimeGreen;
                        }
                        else
                        {
                            tcDataGridView.Rows[i].Cells["Status"].Value = "FAIL";
                            tcDataGridView.Rows[i].Cells["Status"].Style.BackColor = Color.Red;
                            tcDataGridView.Rows[i].Cells["Status"].Style.SelectionBackColor = Color.Red;
                        }
                    }
                }
            }
        }

        /*
         * To send the data of the selected test case
         */
        private void sendSelectedTestData(TreeNodeCollection tNode)
        {
            foreach (TreeNode child in tNode)
            {
                changeNodeIcons(child, (int)Images.UNDETERMINED);
                if (isRunningTestCancel())
                    break;

                if (child.Checked)
                {
                    if (child.Level > 2)
                        sendData(child);

                    if (child.Nodes.Count > 0)
                    {
                        if(child.Level == 2)
                            printToLog(child.Text);

                        sendSelectedTestData(child.Nodes);
                        // Note : the comparison functions will located in the ondatareceived function
                    }
                }
            }
        }   

        /*
         * Send data to the terminal
         */
        private void sendData(TreeNode child)
        {
            string cmd;
            highlightSelectedNode(child);
            List<SequenceNum> sqNo = getNodeSeqNum(child);

            if(sqNo != null)
            {
                printToLog(child.Text);
                for(int i = 0; i < sqNo.Count; i++)
                {
                    if (isRunningTestCancel())
                        break;

                    // send the command to the client
                    cmd = createDiagCommand(sqNo[i].getDiagCmd(), sqNo[i].getPara());
                    seqIndex = i;
                    terminalBusy = true;
                    SendDataToClient(cmd, handler);

                    while (terminalBusy)
                    {
                        if(isRunningTestCancel())
                            break;
                    }
                }
            }
        }

        /*
         * Check running test backgroud worker status
         */
        private bool isRunningTestCancel()
        {
            if (RunningTest.CancellationPending == true)
            {
                //if cancel then straight away report progress completed
                RunningTest.ReportProgress(100);
                return true;
            }

            return false;
        }

        /*
         * Get the node test case info
         */
        private List<TestCase> getNodeTestCase(TreeNode node)
        {
            try
            {
                for (int i = 0; i < tm.Count; i++)
                {
                    if (string.Equals(node.Parent.Text, tm[i].getModule()))
                    {
                        return tm[i].getTestCase();
                    }
                }
            }
            catch (Exception ex)
            {
                showMsgBox(ex.Message, MessageBoxIcon.Error);
            }

            return null;
        }

        /*
         * Get the lowest level test case node's SequenceNum info
         */
        private List<SequenceNum> getNodeSeqNum(TreeNode node)
        {
            try
            {
                for (int i = 0; i < tm.Count; i++)
                {
                    if (string.Equals(node.Parent.Text, tm[i].getModule()))
                    {
                        List<TestCase> testCase = tm[i].getTestCase();
                        return testCase[node.Index].getSeqNum();
                    }
                }
            }
            catch (Exception ex)
            {
                showMsgBox(ex.Message, MessageBoxIcon.Error);
            }

            return null;
        }
        
        /*
         * Handle the message printing to the log textbox
         */
        public void printToLog(string msg)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((MethodInvoker)delegate
                {                   
                    logTxtBox.AppendText(Environment.NewLine + msg);

                    if (tbSaveLog.ToggleState == ToggleButton.ToggleButtonState.ON)
                    {
                        if (!string.IsNullOrEmpty(logFilePath))
                        {
                            logWriter = new StreamWriter(logFilePath, true);
                            logWriter.WriteLine(msg);
                            logWriter.Close();
                        }
                    }
                });
            }
            else
            {
                logTxtBox.AppendText(Environment.NewLine + msg);

                if (tbSaveLog.ToggleState == ToggleButton.ToggleButtonState.ON)
                {
                    if (!string.IsNullOrEmpty(logFilePath))
                    {
                        logWriter = new StreamWriter(logFilePath, true);
                        logWriter.WriteLine(msg);
                        logWriter.Close();
                    }
                }
            }              
        }

        /*
         * Handle the timeout checking
         */
        public void checkTimeout()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    if (isTestStarted == true)
                        timer = new System.Threading.Timer(OnTimer, null, timeout, Timeout.Infinite);
                });
            }
            else
            {
                if (isTestStarted == true)
                    timer = new System.Threading.Timer(OnTimer, null, timeout, Timeout.Infinite);
            }
        }

        /*
         * Handle the icons changing of tree nodes
         */
        private void changeNodeIcons(TreeNode node, int pictureIndex)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    node.ImageIndex = pictureIndex;
                    node.SelectedImageIndex = pictureIndex;
                });
            }
            else
            {
                node.ImageIndex = pictureIndex;
                node.SelectedImageIndex = pictureIndex;
            }
        }

        /*
         * Handle the icons changing of test button
         */
        private void changeTestButtonIcon(bool flag)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    if (flag == true)
                        startTestButton.Image = startTestButton.Image = (Image)Properties.Resources.ResourceManager.GetObject("stop_test");
                    else
                        startTestButton.Image = startTestButton.Image = (Image)Properties.Resources.ResourceManager.GetObject("play");
                });
            }
            else
            {
                if (flag == true)
                    startTestButton.Image = startTestButton.Image = (Image)Properties.Resources.ResourceManager.GetObject("stop_test");     
                else
                    startTestButton.Image = startTestButton.Image = (Image)Properties.Resources.ResourceManager.GetObject("play");
            }
        }

        /*
         * Handle the icons changing of test button name
         */
        private void changeTestButtonName(string name)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    startTestButton.Text = name;      // change button name to stop when button is clicked
                });
            }
            else
                startTestButton.Text = name; 
        }

        /*
         * Highlight node that is currently testing when start test
         */
        private void highlightSelectedNode(TreeNode node)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    myTreeView.SelectedNode = myTreeView.Nodes["TestMenu"].Nodes[node.Parent.Parent.Index].Nodes[node.Parent.Index].Nodes[node.Index];
                    myTreeView.Focus();
                });
            }
            else
            {
                myTreeView.SelectedNode = myTreeView.Nodes["TestMenu"].Nodes[node.Parent.Parent.Index].Nodes[node.Parent.Index].Nodes[node.Index];
                myTreeView.Focus();
            }
        }

        /*
         * This function will start the GUI server and wait for connection
         */
        private void startServer()
        {
            try
            {
                startTestButton.Enabled = true;
                StartServerButton.Enabled = false;
                StopServerButton.Enabled = true;

                // Create the listening socket...
                printToLog("Starting the server...");
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, portNumber);
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // Bind to local IP Address...
                listener.Bind(localEndPoint);
                serverStatus = "Running";
                printToLog("Server started successfully.");

                // Start listening...
                listener.Listen(100);
                printToLog("Waiting for connection on port " + portNumber +"...");

                // Create the call back for any client connections...
                listener.BeginAccept(new AsyncCallback(OnClientConnect), listener);
            }
            catch(ArgumentOutOfRangeException)
            {
                showMsgBox("Cannot start server. Port value out of acceptable range", MessageBoxIcon.Error);
                printToLog("Server failed to start.");
                StartServerButton.Enabled = true;
                StopServerButton.Enabled = false;
                startTestButton.Enabled = false;
            }
            catch (SocketException se)
            {
                showMsgBox(se.Message, MessageBoxIcon.Error);
            }
        }

        /*
         * This is the call back function, which will be invoked when a client is connected
         */
        public void OnClientConnect(IAsyncResult asyn)
        {
            try
            {
                handler = listener.EndAccept(asyn);
                StateObject state = new StateObject();
                handler.ReceiveTimeout = timeout;
                state.workSocket = handler;
                printToLog("Device connected.");
                clientConnected = true;

                waitForDataFromClient(state);

                // Since the listener is now free, it can go back and wait for
                // other clients who are attempting to connect
                listener.BeginAccept(new AsyncCallback(OnClientConnect), null);
            }
            catch(ArgumentException se)
            {
                showMsgBox(se.Message, MessageBoxIcon.Error);
            }
            catch (ObjectDisposedException)
            {
                clientConnected = false;
            }
            catch (SocketException se)
            {
                showMsgBox(se.Message, MessageBoxIcon.Error);
            }
        }

        void OnTimer(object obj)
        {
            showMsgBox("Timeout reached. Terminal probably not responding.", MessageBoxIcon.Error);
            isTestStarted = false;
            timer.Dispose();
            cmd.diagCommand = string.Empty;
            cmd.responceCode = -1;
            cmd.returnData = "TIMEOUT";
            getSelectedNode();
            RunningTest.CancelAsync();
        }

        /*
         * Start waiting for data from the client
         */
        public void waitForDataFromClient(StateObject soc)
        {
            try
            {
                if (pfnWorkerCallBack == null)
                {
                    // Specify the call back function which is to be 
                    // invoked when there is any write activity by the 
                    // connected client
                    pfnWorkerCallBack = new AsyncCallback(OnDataReceived);
                }
                soc.workSocket.BeginReceive(soc.buffer, 0, 10025, 0, pfnWorkerCallBack, soc);
            }
            catch (SocketException)
            {
                clientConnected = false;
            }
        }

        void SendDataToClient(string data, Socket socket)
        {
            // Convert the reply to byte array
            byte[] byData = System.Text.Encoding.ASCII.GetBytes(data);
            printToLog(">> Sent    : " + data.Substring(0, data.Count() - 1));
            socket.Send(byData);
            checkTimeout();
        }

        /*
         * This the call back function which will be invoked when the socket
         * detects any client writing of data on the stream
         */
        public void OnDataReceived(IAsyncResult asyn)
        {
            try
            {
                string content = string.Empty;
                StateObject state = (StateObject)asyn.AsyncState;
                Socket handler = state.workSocket;

                int bytesRead = handler.EndReceive(asyn);

                if (bytesRead > 0)
                {
                    timer.Dispose();
                    dataReceive += (Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                    //To determine if the data receive is end with terminator
                    if (dataReceive[dataReceive.Count() - 1] != '\n')
                    {
                        printToLog(">> Waiting for complete reply...");
                        checkTimeout();
                        return;
                    }

                    // All the data has been read from the 
                    // client. Display it on the console.
                    printToLog(">> Receive : " + dataReceive.Substring(0, dataReceive.Count() - 1));
                    decodeReplyFromClient(dataReceive);
                    dataReceive = string.Empty;

                    // do comparisons here
                    getSelectedNode();
                }
                waitForDataFromClient(state);
            }
            catch (ObjectDisposedException )
            {
                clientConnected = false;
            }
            catch (SocketException)
			{
                printToLog("Device disconnected.");
                clientConnected = false;
			}
            catch(Exception)
            {
                showMsgBox("Test not started", MessageBoxIcon.Warning);
            }
        }

        /*
         * Get selected node of the tree view with this method
         * to avoid cross access and compare result
         */
        private void getSelectedNode()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    compareDiagCmd(myTreeView.SelectedNode);
                    terminalBusy = false;
                });
            }
            else
            {
                compareDiagCmd(myTreeView.SelectedNode);
                terminalBusy = false;
            }
        }

        /*
         * Do some diagnostic command comparison on data sent and receive from terminal
         */
        private void compareDiagCmd(TreeNode node)
        {
            List<SequenceNum> sqNo = getNodeSeqNum(node);

            if (sqNo != null)
            {
                sqNo[seqIndex].setActual(cmd.returnData);
                tcDataGridView.Rows[seqIndex].Cells["Response"].Value = sqNo[seqIndex].getActual();

                if (string.Equals(sqNo[seqIndex].getDiagCmd(), cmd.diagCommand))
                {
                    if (compareResult(node, sqNo[seqIndex]))
                    {
                        sqNo[seqIndex].setResult(1);
                        tcDataGridView.Rows[seqIndex].Cells["Status"].Value = "PASS";
                        tcDataGridView.Rows[seqIndex].Cells["Status"].Style.BackColor = Color.LimeGreen;
                        tcDataGridView.Rows[seqIndex].Cells["Status"].Style.SelectionBackColor = Color.LimeGreen;
                    }
                    else
                    {
                        sqNo[seqIndex].setResult(0);
                        tcDataGridView.Rows[seqIndex].Cells["Status"].Value = "FAIL";
                        tcDataGridView.Rows[seqIndex].Cells["Status"].Style.BackColor = Color.Red;
                        tcDataGridView.Rows[seqIndex].Cells["Status"].Style.SelectionBackColor = Color.Red;
                    }
                }
                else
                {
                    /* For Debugging Purposes */
                    //if (!string.IsNullOrEmpty(cmd.diagCommand))
                    //    MessageBox.Show("Diagnostic command receive is different with the sent diagnostic command");

                    tcDataGridView.Rows[seqIndex].Cells["Status"].Value = "FAIL";
                    tcDataGridView.Rows[seqIndex].Cells["Status"].Style.BackColor = Color.Red;
                    sqNo[seqIndex].setResult(0);
                    changeNodeIcons(node, (int)Images.FAIL);
                    changeNodeIcons(node.Parent, (int)Images.FAIL);
                    changeNodeIcons(node.Parent.Parent, (int)Images.FAIL);
                    testSettingStatus();
                }
                updateClasses(node, sqNo);
            }
        }

        /*
         * Do some result comparison and update the image status of
         * the treeview node to indicate the result after test
         */
        private bool compareResult(TreeNode node, SequenceNum sqNum)
        {
            if (cmd.responceCode == 0)
            {
                if (string.Equals(sqNum.getExpected() + "\n", cmd.returnData))
                {
                    determineNodeImageStatus(node, !failTest);
                    return true;
                }
                else if (sqNum.getExpected().After(":").Equals("PROMPT_USER"))
                {

                    DialogResult result = MessageBox.Show("Did the result generated as describe?\n" +
                                                          "Description: " + sqNum.getDesc() +
                                                          "\n" + cmd.returnData.After(":"),
                                                          "Confirmation", MessageBoxButtons.YesNo);
                    if (result == DialogResult.No)
                    {
                        determineNodeImageStatus(node, failTest);
                        testSettingStatus();
                    }
                    else
                    {
                        determineNodeImageStatus(node, !failTest);
                        return true;
                    }
                }
                else
                {
                    determineNodeImageStatus(node, failTest);
                    testSettingStatus();
                }
            }
            else
            {
                determineNodeImageStatus(node, failTest);
                testSettingStatus();
            }

            return false;
        }

        /*
         * To determine and change the test status of node image
         */
        private void determineNodeImageStatus(TreeNode node, bool testStatus)
        {
            if (testStatus)
            {
                if (node.ImageIndex != (int)Images.FAIL)
                    changeNodeIcons(node, (int)Images.PASS);

                if (node.Parent.ImageIndex != (int)Images.FAIL)
                {
                    changeNodeIcons(node.Parent, (int)Images.PASS);
                    changeNodeIcons(node.Parent.Parent, (int)Images.PASS);
                }
            }
            else
            {
                changeNodeIcons(node, (int)Images.FAIL);
                changeNodeIcons(node.Parent, (int)Images.FAIL);
                changeNodeIcons(node.Parent.Parent, (int)Images.FAIL);
            }
        }

        /*
         * Continue or stop the test according to test setting set by user
         */
        private void testSettingStatus()
        {
            switch (settingForTest)
            {
                case "Halt":
                    RunningTest.CancelAsync();
                    break;
                case "Prompt":
                    DialogResult result = MessageBox.Show("Would you like to continue the tests?", "Test Setting",
                                                          MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.No)
                    {
                        RunningTest.CancelAsync();
                    }
                    break;
                default:
                    break;
            }
        }

        /*
         * Update the classes after receive response from terminal
         */
        private void updateClasses(TreeNode node, List<SequenceNum> sqNum)
        {
            for (int i = 0; i < tm.Count; i++)
            {
                if (string.Equals(node.Parent.Text, tm[i].getModule()))
                {
                    List<TestCase> testCase = tm[i].getTestCase();
                    testCase[node.Index].setSeqNum(sqNum);
                    tm[i].setTestCase(testCase);
                }
            }
        }

        /*
         * Handle the application exit
         * Shutdown all the background work in order to prevent disposed exception
         */
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            keepTrackHostClient.CancelAsync();

            if(keepTrackHostClient.IsBusy == false)
            {
                keepTrackHostClient.Dispose();
            }
            watcher.Dispose();
        }

        private void keepTrackHostClient_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            testSettingStatuslbl.Text = "Test Setting : " + settingForTest;
            serverStatuslbl.Text = "Server Status : " + serverStatus;
            IPStatuslbl.Text = "Host IP : " + IpAddress;
            PortStatuslbl.Text = "Port : " + portNumber;
        }

        /*
         * Change port number
         */
        private void portToolStripMenuItem_Click(object sender, EventArgs e)
        {
            handlePortChange();
        }

        private void handlePortChange()
        {
            GetPortInput portInput = new GetPortInput(serverStatus, portNumber);
            portInput.ShowDialog();

            if (portInput.getNewPortNumber() != -1)          // this is to prevent empty port number being written into 
                portNumber = portInput.getNewPortNumber();

            if (serverStatus == "Running")
            {
                if (portInput.restartServer == "YES")
                {
                    portInput.restartServer = "NO";
                    stopServer();
                    startServer();
                }
            }
        }
        
        private void enterNewPortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            handleTestSettingSelection();
        }

        private void handleTestSettingSelection()
        {
            TestSetting setting = new TestSetting();
            setting.ShowDialog();

            if (setting.getChoice() != "cancelIsClicked")
                settingForTest = setting.getChoice();
        }

        private void StartServerButton_Click(object sender, EventArgs e)
        {
            startServer();
        }

        private void StopServerButton_Click(object sender, EventArgs e)
        {
            stopServer();
        }

        /*  
         * Function to handle all the relevant and 
         * neccessary actions for stopping the server
         */
        private void stopServer()
        {
            printToLog("Server is shutting down.");
            if (handler != null)
                handler.Close();                     //close the server
            if (listener != null)
                listener.Close();
            StartServerButton.Enabled = true;
            StopServerButton.Enabled = false;
            startTestButton.Enabled = false;
            printToLog("Server successfully shutdown.");
            serverStatus = "Offline";
        }

        private void startTestButton_Click(object sender, EventArgs e)
        {
            totalFailTest = 0;
            totalPassTest = 0;

            if (folderXMLDir == string.Empty && singleXMLDir == string.Empty)
            {
                showMsgBox("No valid xml is selected.", MessageBoxIcon.Warning);
            }
            else if(Convert.ToInt16(lblTotalTest.Text) == 0)
            {
                showMsgBox("No test case selected.", MessageBoxIcon.Warning);
            }
            else
            {
                if( clientConnected != true)
                {
                    showMsgBox("No device connected.", MessageBoxIcon.Warning);
                }
                else
                {
                    watcher.EnableRaisingEvents = false;              // disable the file watching
                    isTestStarted = !isTestStarted;                   // toggle the test started flag
                    if(isTestStarted == true)
                    {
                        StartServerButton.Enabled = false;
                        StopServerButton.Enabled = false;
                        tbSaveLog.Enabled = false;

                        // Create log folder if folder not exist
                        if (File.Exists(Directory.GetCurrentDirectory() + "\\Log"))
                            Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\Log");

                        // Generate log file path
                        logFilePath = Path.Combine(Directory.GetCurrentDirectory(), "Log") + "\\Log_" +
                                      DateTime.Now.ToString("dd-MM-yyyy HH_mm_ss") + ".txt";

                        printToLog("Testing started.");
                        RunningTest.RunWorkerAsync();
                    }
                    else
                    {
                        timer.Dispose();
                        RunningTest.CancelAsync();
                    }                 
                }
            }
        }
        
        /*
         * This function will handle the background operation of keep the test running
         */
        private void RunningTest_DoWork(object sender, DoWorkEventArgs e)
        {
            // Do work here
            changeTestButtonName("Stop");
            changeTestButtonIcon(isTestStarted);
            sendSelectedTestData(myTreeView.Nodes);         
        }

        private void RunningTest_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                startTestButton.Enabled = false;
                this.Cursor = Cursors.WaitCursor;

                if( File.Exists(filename + ".sqlite") == false)
                    db.createDatabase(filename);

                connection = db.createDBConnection(filename, connection);
                db.connectToDB(connection);
                db.createTestCasesTable(connection);
                writeTestResultToDB(myTreeView.Nodes);
                db.closeConnection(connection);
            }
            catch (Exception ex)
            {
                showMsgBox(ex.Message, MessageBoxIcon.Error);
            }

            if (isTestStarted == true)
            {
                // Show a dialog box that confirms the process has complete
                isTestStarted = !isTestStarted;
                MessageBox.Show(new Form() { TopMost = true }, "Test completed. Check summary for details.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                printToLog("Test run completed.");
            }
            else
                printToLog("Testing canceled.");

            RunningTest.Dispose();
            this.Cursor = Cursors.Default;
            changeTestButtonName("Start Test");
            changeTestButtonIcon(isTestStarted);
            startTestButton.Enabled = true;
            StopServerButton.Enabled = true;

            determineTotalPassAndFailTests(myTreeView.Nodes);

            if (totalFailTest > 0)
                changeNodeIcons(myTreeView.Nodes["TestMenu"], (int)Images.FAIL);
            else if (totalFailTest == 0 && totalPassTest > 0)
                changeNodeIcons(myTreeView.Nodes["TestMenu"], (int)Images.PASS);

            lblPassTest.Text = totalPassTest.ToString();
            lblFailTest.Text = totalFailTest.ToString();
            logFilePath = string.Empty;
            tbSaveLog.Enabled = true;
            watcher.EnableRaisingEvents = true;              // enable the file watching
        }

        /*
         * Write result to database
         */
        private void writeTestResultToDB(TreeNodeCollection nodes)
        {
            foreach (TreeNode child in nodes)
            {
                if (child.Checked)
                {
                    if (child.Level > 2)
                    {
                        dbOperation(child);
                    }
                    else
                        writeTestResultToDB(child.Nodes);
                }
            }
        }

        /*
         * Add data to the database file
         */
        private void dbOperation(TreeNode node)
        {
            string command;
            List<SequenceNum> sqNo = getNodeSeqNum(node);

            string test_update = "UPDATE TestCases SET expected = @expected , actual = @actual , result = @result Where command = @itemToSearch";
            string test_add = "insert into TestCases (command, expected, actual, result) values ( @command, @expected, @actual, @result)";
            SQLiteCommand add = new SQLiteCommand(test_add, connection);
            SQLiteCommand update = new SQLiteCommand(test_update, connection);
            SQLiteTransaction trans = connection.BeginTransaction();

            if (sqNo != null)
            {
                for (int i = 0; i < sqNo.Count; i++)
                {
                    command = generatedCommandString(node, sqNo[i].getSeqNo(), sqNo[i].getDiagCmd());
                    if (db.isRecordExists(connection, command) == false)
                    {
                        db.addData(command, sqNo[i].getExpected(), sqNo[i].getActual(), sqNo[i].getResult(), add);
                    }  
                    else
                        db.updateSpecificDataInDB(update, command, sqNo[i].getExpected(), sqNo[i].getActual(), sqNo[i].getResult());
                }
            }

            trans.Commit();
        }

        /*
         * Concatenate the string with category, module, test case number and sequence number
         */
        private string generatedCommandString(TreeNode node, string sn, string diagCmd)
        {
            for (int i = 0; i < tm.Count; i++)
            {
                if (string.Equals(node.Parent.Text, tm[i].getModule()))
                {
                    List<TestCase> testCase = tm[i].getTestCase();
                    return diagCmd.Substring(0, 4) + ":" + testCase[node.Index].getTcNo() + "|" + sn;
                }
            }

            return null;
        }

        /*
         * This event is not avalaible as mouse double click event is disable
         * as well as this to prevent error of during test start
         */
        private void myTreeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                if (!isTestStarted)
                {
                    myTreeView.SelectedNode.Checked = !myTreeView.SelectedNode.Checked;
                    CheckAllChildNodes(myTreeView.SelectedNode, myTreeView.SelectedNode.Checked);
                }
            }
            catch(Exception ex) { }
        }

        private void autochkBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkBx = sender as CheckBox;

            if (myTreeView.Nodes.Count > 0)
            {
                checkAllSelectionTests(myTreeView.Nodes, chkBx.Checked, !userVerifiedTest);
            }
        }

        private void manualchkBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkBx = sender as CheckBox;

            if (myTreeView.Nodes.Count > 0)
            {
                checkAllSelectionTests(myTreeView.Nodes, chkBx.Checked, userVerifiedTest);
            }
        }

        /*
         * Select or unselect all the manual or auto test
         */
        private void checkAllSelectionTests(TreeNodeCollection nodes, bool nodeChecked, bool isUserVerified)
        {
            foreach (TreeNode node in nodes)
            {
                if (isSelectionTest(node, isUserVerified))
                {
                    node.Checked = nodeChecked;
                    updateParentNodeCheck(node);

                    if(node.Parent.Checked)
                    {
                        expandCollapseNode(node.Parent, node.Parent.Checked);
                        expandCollapseNode(node.Parent.Parent, node.Parent.Parent.Checked);
                        expandCollapseNode(node.Parent.Parent.Parent, node.Parent.Parent.Parent.Checked);
                    }
                }

                if (node.Nodes.Count > 0)
                {
                    checkAllSelectionTests(node.Nodes, nodeChecked, isUserVerified);
                }
            }
        }

        /*
         * Update current selected manual or auto test
         */
        //private void updateTotalSelectionTest(TreeNode node, bool nodeChecked)
        //{
        //    if (isSelectionTest(node, userVerifiedTest))
        //    {
        //        if (nodeChecked)
        //            currentNoOfManualTest++;
        //        else
        //        {
        //            if (currentNoOfManualTest > 0)
        //                currentNoOfManualTest--;
        //        }
        //    }
        //    else if (isSelectionTest(node, !userVerifiedTest))
        //    {
        //        if (nodeChecked)
        //            currentNoOfAutoTest++;
        //        else
        //        {
        //            if (currentNoOfAutoTest > 0)
        //                currentNoOfAutoTest--;
        //        }
        //    }
        //}

        /*
         * To check whether test case is under which type of test for each test menu
         */
        private bool isSelectionTest(TreeNode node, bool isUserVerified)
        {
            if (node.Level > 2)
            {
                for (int i = 0; i < tm.Count; i++)
                {
                    if (string.Equals(node.Parent.Text, tm[i].getModule()))
                    {
                        List<TestCase> testCase = tm[i].getTestCase();

                        if ((isTestCaseUserVerified(testCase, node.Index) && isUserVerified) ||
                            (!isTestCaseUserVerified(testCase, node.Index) && !isUserVerified))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /*
         * To return the total number of manual or auto test
         */
        private int numOfAllSelectionTestCheck(TreeNodeCollection nodes, bool isUserVerified)
        {
            int numOfTestCheck = 0;

            foreach (TreeNode node in nodes)
            {
                if (node.Level > 2)
                {
                    if(isSelectionTest(node, isUserVerified))
                    {
                        numOfTestCheck++;
                    }
                }

                if (node.Nodes.Count > 0)
                {
                    numOfTestCheck += numOfAllSelectionTestCheck(node.Nodes, isUserVerified);
                }
            }

            return numOfTestCheck;
        }

        /*
         * To check whether sequence number contain any PROMPT_USER for each test case
         */
        private bool isTestCaseUserVerified(List<TestCase> testCase, int nodeIndex)
        {
            try
            {
                List<SequenceNum> sqNum = testCase[nodeIndex].getSeqNum();

                for (int i = 0; i < sqNum.Count; i++)
                {
                    if (string.Equals(sqNum[i].getExpected().After(":"), "PROMPT_USER"))
                        return true;
                }
            }
            catch(Exception ex)
            {
                showMsgBox(ex.Message, MessageBoxIcon.Error);
            }

            return false;
        }

        private void myTreeView_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            if (isTestStarted)
                e.Cancel = true;
            else
                e.Cancel = false;
        }

        private void myTreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (isTestStarted)
                e.Cancel = true;
            else
                e.Cancel = false;
        }

        private void myTreeView_BeforeCheck(object sender, TreeViewCancelEventArgs e)
        {
            if (isTestStarted)
                e.Cancel = true;
            else
                e.Cancel = false;
        }

        private void PortStatuslbl_DoubleClick(object sender, EventArgs e)
        {
            handlePortChange();
        }

        private void testSettingStatuslbl_DoubleClick(object sender, EventArgs e)
        {
            handleTestSettingSelection();
        }


        /* 
         * This function will create the diagnostic command in the correct format 
         */
        private string createDiagCommand(string diagCmd, string param)
        {
            string command = string.Empty;

            if (string.IsNullOrEmpty(param))
                command += diagCmd + "\n";
            else
                command += diagCmd + ":" + param + "\n";

            return command;
        }

        /*  
         * This function will decode the reply message from 
         * the client. The decoded message will be stored inside a structure
         * and returned
         */
        private void decodeReplyFromClient(string reply)
        {
            try
            {
                cmd.diagCommand = reply.Before("=");
                cmd.returnData = reply.After("=");
                int index = reply.IndexOf('=');
                cmd.responceCode = Convert.ToInt32(reply.Substring(index + 1, 2));

            }
            catch (Exception ex) { showMsgBox("Error on data format receive from terminal\n" + reply, MessageBoxIcon.Error); }
        }

        /*
         * 
         */
        private void decodeDatabase(string info, string actualData, int status)
        {
            try
            {
                returnData.diagCommand = info.Before(":");
                returnData.testCase = info.Between(":", "|");
                returnData.seqNumber = info.After("|");
                returnData.actual = actualData;
                returnData.result = status;
            }
            catch (Exception ex) { showMsgBox("Error on database info", MessageBoxIcon.Error); }
        }

        /*
         * 
         */
        private void loadDatabaseIntoTree()
        {
            totalFailTest = 0;
            totalPassTest = 0;

            if (File.Exists("database.sqlite") )
            { 
                string sql = "SELECT * FROM TestCases";
                connection = db.createDBConnection(filename, connection);
                db.connectToDB(connection);
                db.createTestCasesTable(connection);
                SQLiteCommand command = new SQLiteCommand(sql, connection);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    decodeDatabase(reader["command"].ToString(), reader["actual"].ToString(), Convert.ToInt32(reader["result"]));
                    
                    // determine which node is it
                    if (myTreeView.Nodes.Count > 0)
                    {
                        try
                        {
                            updateTreeViewFromDB(myTreeView.Nodes["TestMenu"].Nodes[categoryName(returnData.diagCommand[0])].Nodes[moduleName(returnData.diagCommand.Substring(1,3))].Nodes);
                        }
                        catch (Exception ex) { }
                    }
                }

                determineTotalPassAndFailTests(myTreeView.Nodes);

                if (totalFailTest > 0)
                    changeNodeIcons(myTreeView.Nodes["TestMenu"], (int)Images.FAIL);
                else if (totalFailTest == 0 && totalPassTest > 0)
                    changeNodeIcons(myTreeView.Nodes["TestMenu"], (int)Images.PASS);

                lblPassTest.Text = totalPassTest.ToString();
                lblFailTest.Text = totalFailTest.ToString();
                db.closeConnection(connection);
            }
            else
            {
                db.createDatabase(filename);
                connection = db.createDBConnection(filename, connection);
                db.connectToDB(connection);
                db.createTestCasesTable(connection);
                db.closeConnection(connection);
            }
        }

        /*
         * Update tree view node response and result from database
         */
        private void updateTreeViewFromDB(TreeNodeCollection nodes)
        {
            foreach(TreeNode node in nodes)
            {
                if(node.Level > 2)
                {
                    List<SequenceNum> sqNum = getNodeSeqNum(node);
                    if (updateNode(node, sqNum))
                    {
                        if (node.Parent.Checked)
                        {
                            expandCollapseNode(node.Parent, node.Parent.Checked);
                            expandCollapseNode(node.Parent.Parent, node.Parent.Parent.Checked);
                            expandCollapseNode(node.Parent.Parent.Parent, node.Parent.Parent.Parent.Checked);
                        }
                        return;
                    }
                }
            }
        }

        /*
         * To update the node image status from database
         */
        private bool updateNode(TreeNode node, List<SequenceNum> sqNo)
        {
            for(int i = 0; i < sqNo.Count; i++)
            {
                if(sqNo[i].getSeqNo().Equals(returnData.seqNumber) && node.Text.Equals(returnData.testCase))
                {
                    sqNo[i].setActual(returnData.actual);
                    sqNo[i].setResult(returnData.result);
                    node.Checked = true;
                    updateParentNodeCheck(node);

                    if (sqNo[i].getResult() != -1)
                    {
                        if (sqNo[i].getResult() == 1)
                        {
                            determineNodeImageStatus(node, !failTest);
                        }
                        else
                        {
                            determineNodeImageStatus(node, failTest);
                        }
                    }

                    updateClasses(node, sqNo);
                    return true;
                }
            }

            return false;
        }

        /*
         * To check whether the category id match the category name
         */
        //private bool isThisCategory(char categoryID, string categoryName)
        //{
        //    bool result = false;

        //    switch(categoryID)
        //    {
        //        case 'D':
        //            if (categoryName.Equals("Driver"))
        //                result = true;
        //            break;
        //        case 'K':
        //            if (categoryName.Equals("Kernel"))
        //                result = true;
        //            break;
        //        case 'L':
        //            if (categoryName.Equals("Library"))
        //                result = true;
        //            break;
        //        case 'A':
        //            if (categoryName.Equals("Application"))
        //                result = true;
        //            break;
        //        case 'V':
        //            if (categoryName.Equals("View"))
        //                result = true;
        //            break;
        //        case 'T':
        //            if (categoryName.Equals("TNG"))
        //                result = true;
        //            break;
        //        case 'E':
        //            if (categoryName.Equals("E-Debit"))
        //                result = true;
        //            break;
        //        default:
        //            break;
        //    }

        //    return result;
        //}

        /*
         * To check whether the module id match the module name
         */
        //private bool isThisModule(string moduleID, string moduleName)
        //{
        //    bool result = false;

        //    switch(moduleID)
        //    {
        //        case "BUZ":
        //            if (moduleName.Equals("Buzzer"))
        //                result = true;
        //            break;
        //        case "CLR":
        //            if (moduleName.Equals("Contactless"))
        //                result = true;
        //            break;
        //        case "CRY":
        //            if (moduleName.Equals("Crypto"))
        //                result = true;
        //            break;
        //        case "ETH":
        //            if (moduleName.Equals("Ethenet"))
        //                result = true;
        //            break;
        //        case "FIL":
        //            if (moduleName.Equals("File"))
        //                result = true;
        //            break;
        //        case "GPR":
        //            if (moduleName.Equals("GPRS"))
        //                result = true;
        //            break;
        //        case "KEY":
        //            if (moduleName.Equals("Keypad"))
        //                result = true;
        //            break;
        //        case "LCD":
        //            if (moduleName.Equals("LCD"))
        //                result = true;
        //            break;
        //        case "LED":
        //            if (moduleName.Equals("LED"))
        //                result = true;
        //            break;
        //        case "MAG":
        //            if (moduleName.Equals("MagStripe"))
        //                result = true;
        //            break;
        //        case "MDM":
        //            if (moduleName.Equals("Modem"))
        //                result = true;
        //            break;
        //        case "PPP":
        //            if (moduleName.Equals("Modem PPP"))
        //                result = true;
        //            break;
        //        case "POW":
        //            if (moduleName.Equals("Power"))
        //                result = true;
        //            break;
        //        case "PRT":
        //            if (moduleName.Equals("Printer"))
        //                result = true;
        //            break;
        //        case "RTC":
        //            if (moduleName.Equals("RTC"))
        //                result = true;
        //            break;
        //        case "SER":
        //            if (moduleName.Equals("Serial Port"))
        //                result = true;
        //            break;
        //        case "SCR":
        //            if (moduleName.Equals("Smart Card"))
        //                result = true;
        //            break;
        //        default:
        //            break;
        //    }

        //    return result;
        //}

        /*
         * To return the full name of the category
         */
        private string categoryName(char categoryID)
        {
            switch (categoryID)
            {
                case 'D':
                    return "Driver";
                case 'K':
                    return "Kernel";
                case 'L':
                    return "Library";
                case 'A':
                    return "Application";
                case 'V':
                    return "View";
                case 'T':
                    return "TNG";
                case 'E':
                    return "Debit";
                default:
                    return null;
            }
        }

        /*
         * To return the full name of the module
         */
        private string moduleName(string moduleID)
        {
            switch (moduleID)
            {
                case "BUZ":
                    return "Buzzer";
                case "CLR":
                    return "Contactless";
                case "CRY":
                    return "Crypto";
                case "ETH":
                    return "Ethenet";
                case "FIL":
                    return "File";
                case "GPR":
                    return "GPRS";
                case "KEY":
                    return "Keypad";
                case "LCD":
                    return "LCD";
                case "LED":
                    return "LED";
                case "MAG":
                    return "MagStripe";
                case "MDM":
                    return "Modem";
                case "PPP":
                    return "Modem PPP";
                case "POW":
                    return "Power";
                case "PRT":
                    return "Printer";
                case "RTC":
                    return "RTC";
                case "SER":
                    return "Serial Port";
                case "SCR":
                    return "Smart Card";
                default:
                    return null;
            }
        }

        private void deleteDatabseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Database table will be reset. Confirm this action? Any data lost cannot be recover."
                                               , "", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (dr == DialogResult.Yes)
            {
                string temp = " DROP Table IF EXISTS 'TestCases'";
                connection = db.createDBConnection(filename, connection);
                db.connectToDB(connection);
                SQLiteCommand command = new SQLiteCommand(temp, connection);
                command.ExecuteNonQuery();
                command.CommandText = "vacuum;";        // delete the file residues
                command.ExecuteNonQuery();
                db.closeConnection(connection);
            }
        }

        /*
         * Update the total number of pass and fail tests after test completed
         */
        private void determineTotalPassAndFailTests(TreeNodeCollection nodes)
        {
            foreach(TreeNode node in nodes)
            {
                if (node.Level > 2 && node.Checked)
                {
                    if (node.ImageIndex == (int)Images.FAIL)
                        totalFailTest++;
                    else if (node.ImageIndex == (int)Images.PASS)
                        totalPassTest++;
                }
                else
                    determineTotalPassAndFailTests(node.Nodes);
            }
        }

        private void changeTimeoutDurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeTimeoutDuration t = new ChangeTimeoutDuration(timeout);
            t.ShowDialog();

            int temp = t.getTimeoutDuration();

            if (temp != -1)
                timeout = temp;
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            // Get file name.
            string name = saveFileDialog1.FileName;

            // Write to the file name selected.
            generateReport(name);
        }

        /*
         * To generate the complete report of the test
         */
        private void generateReport(string filename)
        {
            try
            {
                // Write sample data to CSV file
                using (CsvFileWriter writer = new CsvFileWriter(filename))
                {
                    CsvRow row = new CsvRow();
                    row.Add("Category");
                    row.Add("Module");
                    row.Add("Test Case");
                    row.Add("Test Case Description");
                    row.Add("Sequence Number");
                    row.Add("Diagnostic, Command");
                    row.Add("Description");
                    row.Add("Parameter");
                    row.Add("Expected Outcome");
                    row.Add("Response");
                    row.Add("Result");
                    writer.WriteRow(row);
                    writeReport(myTreeView.Nodes, writer, filename);
                }
            }
            catch (Exception ex) { showMsgBox(ex.Message, MessageBoxIcon.Error); }
        }

        /*
         * Write the log to file and save as .csv
         */
        private void writeReport(TreeNodeCollection nodes, CsvFileWriter writer, string filename)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.Checked)
                {
                    if (node.Level > 2)
                    {
                        List<TestCase> tCase = getNodeTestCase(node);
                        List<SequenceNum> sqNum = getNodeSeqNum(node);

                        for (int i = 0; i < sqNum.Count; i++)
                        {
                            CsvRow row = new CsvRow();
                            row.Add(node.Parent.Parent.Text);
                            row.Add(node.Parent.Text);
                            row.Add(node.Text);
                            row.Add(tCase[node.Index].getDesc());
                            row.Add(sqNum[i].getSeqNo());
                            row.Add(sqNum[i].getDiagCmd());
                            row.Add(sqNum[i].getDesc());
                            row.Add(sqNum[i].getPara());
                            row.Add(sqNum[i].getExpected());
                            row.Add(sqNum[i].getActual());

                            if (sqNum[i].getResult() == 1)
                                row.Add("PASS");
                            else if (sqNum[i].getResult() == 0)
                                row.Add("FAIL");

                            writer.WriteRow(row);
                        }

                    }

                    if (node.Nodes.Count > 0)
                    {
                        writeReport(node.Nodes, writer, filename);
                    }
                }
            }
        }

        /*
         * To check whether the folder exist
         */
        private bool isFileExist()
        {
            if(Directory.Exists(Directory.GetCurrentDirectory() + "\\Log"))
                return true;

            return false;
        }

        private void showMsgBox(string content)
        {
            this.Activated -= TestServerGUI_Activated;
            this.Deactivate -= TestServerGUI_Deactivate;
            MessageBox.Show(content);
            this.Deactivate += TestServerGUI_Deactivate;
        }

        private void showMsgBox(string content, MessageBoxIcon iconSelection)
        {
            this.Activated -= TestServerGUI_Activated;
            this.Deactivate -= TestServerGUI_Deactivate;
            MessageBox.Show(content, "", MessageBoxButtons.OK, iconSelection);
            this.Deactivate += TestServerGUI_Deactivate;
        }

        private void TestServerGUI_Activated(object sender, EventArgs e)
        {
            if (isTestStarted == false && watcherResponse == true )
            {
                this.Activated -= TestServerGUI_Activated;
                this.Deactivate -= TestServerGUI_Deactivate;
                MessageBox.Show("One or more XML files has been modified externally. It will be reload automatically.",
                "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                watcherResponse = false;
                this.Cursor = Cursors.WaitCursor;
                this.Deactivate += TestServerGUI_Deactivate;

                if (File.Exists(singleXMLDir) == false && isXmlFolderSelected == false)
                {
                    tm.Clear();
                    tc.Clear();
                    myTreeView.Nodes.Clear();
                    watcher.EnableRaisingEvents = false;
                }
                else
                {
                    tm.Clear();
                    tc.Clear();
                    buildTreeViewFromXML();
                    loadDatabaseIntoTree();
                }

                this.Cursor = Cursors.Default;
            }
            else
            {
                this.Activated -= TestServerGUI_Activated;
                this.Deactivate -= TestServerGUI_Deactivate;
                this.Deactivate += TestServerGUI_Deactivate;
            }
        }

        private void TestServerGUI_Deactivate(object sender, EventArgs e)
        {
            this.Activated += TestServerGUI_Activated;
        }
    }
}
