﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerGUI
{
    public partial class GetPortInput : Form
    {
        private int newPortNumber;  // parameter to store the the new port number
        private string serverStatus = string.Empty;
        public string restartServer = string.Empty;

        public GetPortInput(string status, int portnumber)
        {
            InitializeComponent();
            serverStatus = status;
            textBox1.Text = portnumber.ToString();
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text == "")
                    throw new EmptyInput();

                int temp = Convert.ToInt32(textBox1.Text);

                if (serverStatus == "Running")
                {
                    DialogResult dr = MessageBox.Show("Server is already running and listening to a port, do you want to re-run the server " +
                    "and listen to this new port " + temp + "?"
                    , "", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                    if (dr == DialogResult.Yes)
                    {
                        restartServer = "YES";
                        newPortNumber = temp;
                    }
                    else
                    {
                        newPortNumber = -1;
                        restartServer = "NO";
                    }              
                }
                else
                    newPortNumber = temp;

                this.Hide();
            }
            catch (EmptyInput)
            {

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            newPortNumber = -1;
            this.Hide();
        }

        public int getNewPortNumber()
        {
            return newPortNumber;
        }

        public void setNewPortNumber(int newPortNumber)
        {
            this.newPortNumber = newPortNumber;
        }

        class EmptyInput : Exception
        {
            public EmptyInput()
            {
                MessageBox.Show("Please enter a value!");
            }
        }

    }
}
