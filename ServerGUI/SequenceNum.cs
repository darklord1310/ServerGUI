﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerGUI
{
    public class SequenceNum
    {
        private string sNo;
        private string description;
        private string diagCmd;
        private string para;
        private string expected;
        private string actual;
        private int result;

        public SequenceNum() { }

        public SequenceNum(string no, string desc, string diagCmd, string para, string expected)
        {
            setSeqNo(no);
            setDesc(desc);
            setDiagCmd(diagCmd);
            setPara(para);
            setExpected(expected);
            setResult(-1);
        }

        public string getSeqNo() { return sNo; }
        public string getDesc() { return description; }
        public string getDiagCmd() { return diagCmd; }
        public string getPara() { return para; }
        public string getExpected() { return expected; }
        public string getActual() { return actual; }
        public int getResult() { return result; }

        public void setSeqNo(string no) { sNo = no; }
        public void setDesc(string desc) { description = desc; }
        public void setDiagCmd(string diagCmd) { this.diagCmd = diagCmd; }
        public void setPara(string para) { this.para = para; }
        public void setExpected(string exp) { expected = exp; }
        public void setActual(string act) { actual = act; }
        public void setResult(int result) { this.result = result; }
    }
}
