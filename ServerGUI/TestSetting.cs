﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerGUI
{
    public partial class TestSetting : Form
    {
        private string choice;  // choice is the storage of user selection on 
                                // user desired setting

        public TestSetting()
        {
            InitializeComponent();

            /* Tooltip code for each for the radio button */
            ToolTip haltingTip = new ToolTip();
            haltingTip.ShowAlways = true;
            haltingTip.SetToolTip(haltRadioButton, "Halt immediately when encounter failed test");

            ToolTip ignoreTip = new ToolTip();
            ignoreTip.ShowAlways = true;
            ignoreTip.SetToolTip(continueRadioButton, "Ignore the failed test and continue the testing");

            ToolTip promptTip = new ToolTip();
            promptTip.ShowAlways = true;
            promptTip.SetToolTip(promptRadioButton, "Prompt user when failed test encountered");
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            if (promptRadioButton.Checked == true)
                choice = "Prompt";
            else if (continueRadioButton.Checked == true)
                choice = "Continue";
            else if (haltRadioButton.Checked == true)
                choice = "Halt";

            this.Hide();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            choice = "cancelIsClicked";
            this.Hide();
        }


        public void setChoice(string choice)
        {
            this.choice = choice;
        }

        public string getChoice()
        {
            return choice;
        }

        private void TestSetting_Load(object sender, EventArgs e)
        {
        }
    }
}
