﻿namespace ServerGUI
{
    partial class TestSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.promptRadioButton = new System.Windows.Forms.RadioButton();
            this.continueRadioButton = new System.Windows.Forms.RadioButton();
            this.haltRadioButton = new System.Windows.Forms.RadioButton();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.promptRadioButton);
            this.groupBox1.Controls.Add(this.continueRadioButton);
            this.groupBox1.Controls.Add(this.haltRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(181, 87);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // promptRadioButton
            // 
            this.promptRadioButton.AutoSize = true;
            this.promptRadioButton.Location = new System.Drawing.Point(15, 56);
            this.promptRadioButton.Name = "promptRadioButton";
            this.promptRadioButton.Size = new System.Drawing.Size(58, 17);
            this.promptRadioButton.TabIndex = 2;
            this.promptRadioButton.Text = "Prompt";
            this.promptRadioButton.UseVisualStyleBackColor = true;
            // 
            // continueRadioButton
            // 
            this.continueRadioButton.AutoSize = true;
            this.continueRadioButton.Location = new System.Drawing.Point(15, 33);
            this.continueRadioButton.Name = "continueRadioButton";
            this.continueRadioButton.Size = new System.Drawing.Size(67, 17);
            this.continueRadioButton.TabIndex = 1;
            this.continueRadioButton.Text = "Continue";
            this.continueRadioButton.UseVisualStyleBackColor = true;
            // 
            // haltRadioButton
            // 
            this.haltRadioButton.AutoSize = true;
            this.haltRadioButton.Checked = true;
            this.haltRadioButton.Location = new System.Drawing.Point(15, 10);
            this.haltRadioButton.Name = "haltRadioButton";
            this.haltRadioButton.Size = new System.Drawing.Size(44, 17);
            this.haltRadioButton.TabIndex = 0;
            this.haltRadioButton.TabStop = true;
            this.haltRadioButton.Text = "Halt";
            this.haltRadioButton.UseVisualStyleBackColor = true;
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(27, 118);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 3;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(108, 118);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 4;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // TestSetting
            // 
            this.AcceptButton = this.CancelButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelButton;
            this.ClientSize = new System.Drawing.Size(215, 155);
            this.ControlBox = false;
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.groupBox1);
            this.Name = "TestSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Setting";
            this.Load += new System.EventHandler(this.TestSetting_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton promptRadioButton;
        private System.Windows.Forms.RadioButton continueRadioButton;
        private System.Windows.Forms.RadioButton haltRadioButton;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelButton;
    }
}