﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServerGUI {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ServerGUI {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ServerGUI() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ServerGUI.ServerGUI", typeof(ServerGUI).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Boolean similar to True.
        /// </summary>
        internal static bool Description_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("Description.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Boolean similar to True.
        /// </summary>
        internal static bool DiagnosticCmd_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("DiagnosticCmd.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Boolean similar to True.
        /// </summary>
        internal static bool ExpOutcome_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("ExpOutcome.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Point similar to {X=263,Y=17}.
        /// </summary>
        internal static System.Drawing.Point keepTrackHostClient_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("keepTrackHostClient.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Point similar to {X=148,Y=17}.
        /// </summary>
        internal static System.Drawing.Point menuStrip1_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("menuStrip1.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Boolean similar to True.
        /// </summary>
        internal static bool Parameter_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("Parameter.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Boolean similar to True.
        /// </summary>
        internal static bool Response_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("Response.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Point similar to {X=560,Y=17}.
        /// </summary>
        internal static System.Drawing.Point RunningTest_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("RunningTest.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Point similar to {X=43,Y=57}.
        /// </summary>
        internal static System.Drawing.Point saveFileDialog1_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("saveFileDialog1.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Boolean similar to True.
        /// </summary>
        internal static bool SeqNo_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("SeqNo.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap StartServerButton_Image {
            get {
                object obj = ResourceManager.GetObject("StartServerButton.Image", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap startTestButton_Image {
            get {
                object obj = ResourceManager.GetObject("startTestButton.Image", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Boolean similar to True.
        /// </summary>
        internal static bool Status_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("Status.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Point similar to {X=667,Y=18}.
        /// </summary>
        internal static System.Drawing.Point statusStrip1_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("statusStrip1.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap StopServerButton_Image {
            get {
                object obj = ResourceManager.GetObject("StopServerButton.Image", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Windows.Forms.ImageListStreamer.
        /// </summary>
        internal static System.Windows.Forms.ImageListStreamer TreeNodeIcons_ImageStream {
            get {
                object obj = ResourceManager.GetObject("TreeNodeIcons.ImageStream", resourceCulture);
                return ((System.Windows.Forms.ImageListStreamer)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Point similar to {X=426,Y=17}.
        /// </summary>
        internal static System.Drawing.Point TreeNodeIcons_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("TreeNodeIcons.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
    }
}
