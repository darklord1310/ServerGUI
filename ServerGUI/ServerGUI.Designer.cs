﻿namespace ServerGUI
{
    partial class TestServerGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestServerGUI));
            this.TreeNodeIcons = new System.Windows.Forms.ImageList(this.components);
            this.tcDataGridView = new System.Windows.Forms.DataGridView();
            this.SeqNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiagnosticCmd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Parameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpOutcome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Response = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtBoxDes = new System.Windows.Forms.TextBox();
            this.lblDesc = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.testSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.continueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openXMLFromFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.portConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enterNewPortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.portToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeTimeoutDurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteDatabseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblFailTest = new System.Windows.Forms.Label();
            this.lblPassTest = new System.Windows.Forms.Label();
            this.lblTotalTest = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.keepTrackHostClient = new System.ComponentModel.BackgroundWorker();
            this.RunningTest = new System.ComponentModel.BackgroundWorker();
            this.logTxtBox = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.IPStatuslbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.PortStatuslbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.serverStatuslbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.testSettingStatuslbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.manualchkBox = new System.Windows.Forms.CheckBox();
            this.autochkBox = new System.Windows.Forms.CheckBox();
            this.StopServerButton = new System.Windows.Forms.Button();
            this.StartServerButton = new System.Windows.Forms.Button();
            this.startTestButton = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.lblSaveLog = new System.Windows.Forms.Label();
            this.tbSaveLog = new MyCustomControl.ToggleButton();
            this.myTreeView = new MyCustomControl.MyTreeView();
            ((System.ComponentModel.ISupportInitialize)(this.tcDataGridView)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TreeNodeIcons
            // 
            this.TreeNodeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("TreeNodeIcons.ImageStream")));
            this.TreeNodeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.TreeNodeIcons.Images.SetKeyName(0, "undetermine.gif");
            this.TreeNodeIcons.Images.SetKeyName(1, "pass.gif");
            this.TreeNodeIcons.Images.SetKeyName(2, "fail.gif");
            this.TreeNodeIcons.Images.SetKeyName(3, "play.gif");
            this.TreeNodeIcons.Images.SetKeyName(4, "stop test.gif");
            // 
            // tcDataGridView
            // 
            this.tcDataGridView.AllowUserToAddRows = false;
            this.tcDataGridView.AllowUserToDeleteRows = false;
            this.tcDataGridView.AllowUserToResizeColumns = false;
            this.tcDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tcDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SeqNo,
            this.DiagnosticCmd,
            this.Description,
            this.Parameter,
            this.ExpOutcome,
            this.Response,
            this.Status});
            this.tcDataGridView.Location = new System.Drawing.Point(245, 155);
            this.tcDataGridView.Name = "tcDataGridView";
            this.tcDataGridView.ReadOnly = true;
            this.tcDataGridView.RowHeadersVisible = false;
            this.tcDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tcDataGridView.Size = new System.Drawing.Size(838, 157);
            this.tcDataGridView.TabIndex = 1;
            // 
            // SeqNo
            // 
            this.SeqNo.HeaderText = "Seq. No.";
            this.SeqNo.Name = "SeqNo";
            this.SeqNo.ReadOnly = true;
            this.SeqNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SeqNo.Width = 40;
            // 
            // DiagnosticCmd
            // 
            this.DiagnosticCmd.HeaderText = "Diagnostic Command";
            this.DiagnosticCmd.Name = "DiagnosticCmd";
            this.DiagnosticCmd.ReadOnly = true;
            this.DiagnosticCmd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Description
            // 
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Description.Width = 230;
            // 
            // Parameter
            // 
            this.Parameter.HeaderText = "Parameter";
            this.Parameter.Name = "Parameter";
            this.Parameter.ReadOnly = true;
            this.Parameter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Parameter.Width = 200;
            // 
            // ExpOutcome
            // 
            this.ExpOutcome.HeaderText = "Expected Outcome";
            this.ExpOutcome.Name = "ExpOutcome";
            this.ExpOutcome.ReadOnly = true;
            this.ExpOutcome.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ExpOutcome.Width = 200;
            // 
            // Response
            // 
            this.Response.HeaderText = "Response";
            this.Response.Name = "Response";
            this.Response.ReadOnly = true;
            this.Response.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Response.Width = 200;
            // 
            // Status
            // 
            this.Status.HeaderText = "Result";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Status.Width = 50;
            // 
            // txtBoxDes
            // 
            this.txtBoxDes.BackColor = System.Drawing.SystemColors.Control;
            this.txtBoxDes.Location = new System.Drawing.Point(245, 52);
            this.txtBoxDes.Multiline = true;
            this.txtBoxDes.Name = "txtBoxDes";
            this.txtBoxDes.ReadOnly = true;
            this.txtBoxDes.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtBoxDes.Size = new System.Drawing.Size(499, 86);
            this.txtBoxDes.TabIndex = 2;
            // 
            // lblDesc
            // 
            this.lblDesc.AutoSize = true;
            this.lblDesc.Location = new System.Drawing.Point(242, 36);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(60, 13);
            this.lblDesc.TabIndex = 3;
            this.lblDesc.Text = "Description";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testSettingToolStripMenuItem,
            this.portConnectionToolStripMenuItem,
            this.databaseToolStripMenuItem,
            this.reportToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1097, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // testSettingToolStripMenuItem
            // 
            this.testSettingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.continueToolStripMenuItem,
            this.openXMLFromFolderToolStripMenuItem});
            this.testSettingToolStripMenuItem.Name = "testSettingToolStripMenuItem";
            this.testSettingToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.testSettingToolStripMenuItem.Text = "File";
            // 
            // continueToolStripMenuItem
            // 
            this.continueToolStripMenuItem.Name = "continueToolStripMenuItem";
            this.continueToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.continueToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.continueToolStripMenuItem.Text = "Open single XML";
            this.continueToolStripMenuItem.Click += new System.EventHandler(this.continueToolStripMenuItem_Click);
            // 
            // openXMLFromFolderToolStripMenuItem
            // 
            this.openXMLFromFolderToolStripMenuItem.Name = "openXMLFromFolderToolStripMenuItem";
            this.openXMLFromFolderToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openXMLFromFolderToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.openXMLFromFolderToolStripMenuItem.Text = "Open XML in folder";
            this.openXMLFromFolderToolStripMenuItem.Click += new System.EventHandler(this.openAllXMLToolStripMenuItem_Click);
            // 
            // portConnectionToolStripMenuItem
            // 
            this.portConnectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enterNewPortToolStripMenuItem,
            this.portToolStripMenuItem,
            this.changeTimeoutDurationToolStripMenuItem});
            this.portConnectionToolStripMenuItem.Name = "portConnectionToolStripMenuItem";
            this.portConnectionToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.portConnectionToolStripMenuItem.Text = "Edit";
            // 
            // enterNewPortToolStripMenuItem
            // 
            this.enterNewPortToolStripMenuItem.Name = "enterNewPortToolStripMenuItem";
            this.enterNewPortToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.enterNewPortToolStripMenuItem.Text = "Test Setting";
            this.enterNewPortToolStripMenuItem.ToolTipText = "Determine the testing behaviour";
            this.enterNewPortToolStripMenuItem.Click += new System.EventHandler(this.enterNewPortToolStripMenuItem_Click);
            // 
            // portToolStripMenuItem
            // 
            this.portToolStripMenuItem.Name = "portToolStripMenuItem";
            this.portToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.portToolStripMenuItem.Text = "Change Port";
            this.portToolStripMenuItem.Click += new System.EventHandler(this.portToolStripMenuItem_Click);
            // 
            // changeTimeoutDurationToolStripMenuItem
            // 
            this.changeTimeoutDurationToolStripMenuItem.Name = "changeTimeoutDurationToolStripMenuItem";
            this.changeTimeoutDurationToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.changeTimeoutDurationToolStripMenuItem.Text = "Change Timeout Duration";
            this.changeTimeoutDurationToolStripMenuItem.Click += new System.EventHandler(this.changeTimeoutDurationToolStripMenuItem_Click);
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteDatabseToolStripMenuItem});
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.databaseToolStripMenuItem.Text = "Database";
            // 
            // deleteDatabseToolStripMenuItem
            // 
            this.deleteDatabseToolStripMenuItem.Name = "deleteDatabseToolStripMenuItem";
            this.deleteDatabseToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.deleteDatabseToolStripMenuItem.Text = "Reset Database";
            this.deleteDatabseToolStripMenuItem.Click += new System.EventHandler(this.deleteDatabseToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripMenuItem});
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.reportToolStripMenuItem.Text = "Report";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblFailTest);
            this.groupBox1.Controls.Add(this.lblPassTest);
            this.groupBox1.Controls.Add(this.lblTotalTest);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(781, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(302, 100);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Summary";
            // 
            // lblFailTest
            // 
            this.lblFailTest.AutoSize = true;
            this.lblFailTest.Location = new System.Drawing.Point(127, 72);
            this.lblFailTest.Name = "lblFailTest";
            this.lblFailTest.Size = new System.Drawing.Size(13, 13);
            this.lblFailTest.TabIndex = 5;
            this.lblFailTest.Text = "0";
            // 
            // lblPassTest
            // 
            this.lblPassTest.AutoSize = true;
            this.lblPassTest.Location = new System.Drawing.Point(127, 43);
            this.lblPassTest.Name = "lblPassTest";
            this.lblPassTest.Size = new System.Drawing.Size(13, 13);
            this.lblPassTest.TabIndex = 4;
            this.lblPassTest.Text = "0";
            // 
            // lblTotalTest
            // 
            this.lblTotalTest.AutoSize = true;
            this.lblTotalTest.Location = new System.Drawing.Point(127, 17);
            this.lblTotalTest.Name = "lblTotalTest";
            this.lblTotalTest.Size = new System.Drawing.Size(13, 13);
            this.lblTotalTest.TabIndex = 3;
            this.lblTotalTest.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Failed Test(s)      :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Passed Test(s)    :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Total Test(s)        :";
            // 
            // keepTrackHostClient
            // 
            this.keepTrackHostClient.WorkerReportsProgress = true;
            this.keepTrackHostClient.WorkerSupportsCancellation = true;
            this.keepTrackHostClient.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.keepTrackHostClient.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.keepTrackHostClient_ProgressChanged);
            // 
            // RunningTest
            // 
            this.RunningTest.WorkerReportsProgress = true;
            this.RunningTest.WorkerSupportsCancellation = true;
            this.RunningTest.DoWork += new System.ComponentModel.DoWorkEventHandler(this.RunningTest_DoWork);
            this.RunningTest.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.RunningTest_RunWorkerCompleted);
            // 
            // logTxtBox
            // 
            this.logTxtBox.BackColor = System.Drawing.SystemColors.ControlText;
            this.logTxtBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logTxtBox.ForeColor = System.Drawing.Color.LimeGreen;
            this.logTxtBox.Location = new System.Drawing.Point(245, 327);
            this.logTxtBox.Multiline = true;
            this.logTxtBox.Name = "logTxtBox";
            this.logTxtBox.ReadOnly = true;
            this.logTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logTxtBox.Size = new System.Drawing.Size(838, 192);
            this.logTxtBox.TabIndex = 16;
            this.logTxtBox.Text = "[Log]";
            // 
            // statusStrip1
            // 
            this.statusStrip1.AllowItemReorder = true;
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IPStatuslbl,
            this.PortStatuslbl,
            this.serverStatuslbl,
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel4,
            this.testSettingStatuslbl});
            this.statusStrip1.Location = new System.Drawing.Point(0, 574);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.ShowItemToolTips = true;
            this.statusStrip1.Size = new System.Drawing.Size(1097, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.Stretch = false;
            this.statusStrip1.TabIndex = 18;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // IPStatuslbl
            // 
            this.IPStatuslbl.AutoSize = false;
            this.IPStatuslbl.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.IPStatuslbl.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.IPStatuslbl.Name = "IPStatuslbl";
            this.IPStatuslbl.Size = new System.Drawing.Size(180, 17);
            this.IPStatuslbl.Text = "Host IP : ";
            this.IPStatuslbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PortStatuslbl
            // 
            this.PortStatuslbl.AutoSize = false;
            this.PortStatuslbl.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.PortStatuslbl.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.PortStatuslbl.DoubleClickEnabled = true;
            this.PortStatuslbl.Name = "PortStatuslbl";
            this.PortStatuslbl.Size = new System.Drawing.Size(120, 17);
            this.PortStatuslbl.Text = "Port : ";
            this.PortStatuslbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.PortStatuslbl.ToolTipText = "Double click to change port";
            this.PortStatuslbl.DoubleClick += new System.EventHandler(this.PortStatuslbl_DoubleClick);
            // 
            // serverStatuslbl
            // 
            this.serverStatuslbl.AutoSize = false;
            this.serverStatuslbl.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.serverStatuslbl.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.serverStatuslbl.Name = "serverStatuslbl";
            this.serverStatuslbl.Size = new System.Drawing.Size(170, 17);
            this.serverStatuslbl.Text = "Server Status : ";
            this.serverStatuslbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel3.Text = " ";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.AutoSize = false;
            this.toolStripStatusLabel4.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(475, 17);
            // 
            // testSettingStatuslbl
            // 
            this.testSettingStatuslbl.AutoSize = false;
            this.testSettingStatuslbl.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.testSettingStatuslbl.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.testSettingStatuslbl.DoubleClickEnabled = true;
            this.testSettingStatuslbl.Name = "testSettingStatuslbl";
            this.testSettingStatuslbl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.testSettingStatuslbl.Size = new System.Drawing.Size(140, 17);
            this.testSettingStatuslbl.Text = "Test Setting : ";
            this.testSettingStatuslbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.testSettingStatuslbl.ToolTipText = "Double click to change setting";
            this.testSettingStatuslbl.DoubleClick += new System.EventHandler(this.testSettingStatuslbl_DoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox2.Controls.Add(this.manualchkBox);
            this.groupBox2.Controls.Add(this.autochkBox);
            this.groupBox2.Location = new System.Drawing.Point(12, 502);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(211, 60);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Test Case Selection";
            // 
            // manualchkBox
            // 
            this.manualchkBox.AutoSize = true;
            this.manualchkBox.Location = new System.Drawing.Point(6, 42);
            this.manualchkBox.Name = "manualchkBox";
            this.manualchkBox.Size = new System.Drawing.Size(116, 17);
            this.manualchkBox.TabIndex = 1;
            this.manualchkBox.Text = "Manual Verification";
            this.manualchkBox.UseVisualStyleBackColor = true;
            this.manualchkBox.CheckedChanged += new System.EventHandler(this.manualchkBox_CheckedChanged);
            // 
            // autochkBox
            // 
            this.autochkBox.AutoSize = true;
            this.autochkBox.Location = new System.Drawing.Point(7, 19);
            this.autochkBox.Name = "autochkBox";
            this.autochkBox.Size = new System.Drawing.Size(128, 17);
            this.autochkBox.TabIndex = 0;
            this.autochkBox.Text = "Automatic Verification";
            this.autochkBox.UseVisualStyleBackColor = true;
            this.autochkBox.CheckedChanged += new System.EventHandler(this.autochkBox_CheckedChanged);
            // 
            // StopServerButton
            // 
            this.StopServerButton.Enabled = false;
            this.StopServerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StopServerButton.Image = ((System.Drawing.Image)(resources.GetObject("StopServerButton.Image")));
            this.StopServerButton.Location = new System.Drawing.Point(811, 529);
            this.StopServerButton.Name = "StopServerButton";
            this.StopServerButton.Size = new System.Drawing.Size(136, 33);
            this.StopServerButton.TabIndex = 15;
            this.StopServerButton.Text = "Stop Server";
            this.StopServerButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.StopServerButton.UseVisualStyleBackColor = true;
            this.StopServerButton.Click += new System.EventHandler(this.StopServerButton_Click);
            // 
            // StartServerButton
            // 
            this.StartServerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartServerButton.Image = ((System.Drawing.Image)(resources.GetObject("StartServerButton.Image")));
            this.StartServerButton.Location = new System.Drawing.Point(659, 529);
            this.StartServerButton.Name = "StartServerButton";
            this.StartServerButton.Size = new System.Drawing.Size(136, 33);
            this.StartServerButton.TabIndex = 13;
            this.StartServerButton.Text = "Start Server";
            this.StartServerButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.StartServerButton.UseVisualStyleBackColor = true;
            this.StartServerButton.Click += new System.EventHandler(this.StartServerButton_Click);
            // 
            // startTestButton
            // 
            this.startTestButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.startTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startTestButton.Image = ((System.Drawing.Image)(resources.GetObject("startTestButton.Image")));
            this.startTestButton.Location = new System.Drawing.Point(969, 529);
            this.startTestButton.Name = "startTestButton";
            this.startTestButton.Size = new System.Drawing.Size(116, 33);
            this.startTestButton.TabIndex = 12;
            this.startTestButton.Text = "Start Test";
            this.startTestButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.startTestButton.UseVisualStyleBackColor = true;
            this.startTestButton.Click += new System.EventHandler(this.startTestButton_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "CSV files (*.csv)|*.csv";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // lblSaveLog
            // 
            this.lblSaveLog.AutoSize = true;
            this.lblSaveLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaveLog.Location = new System.Drawing.Point(326, 538);
            this.lblSaveLog.Name = "lblSaveLog";
            this.lblSaveLog.Size = new System.Drawing.Size(68, 17);
            this.lblSaveLog.TabIndex = 21;
            this.lblSaveLog.Text = "Save Log";
            // 
            // tbSaveLog
            // 
            this.tbSaveLog.ActiveColor = System.Drawing.Color.Teal;
            this.tbSaveLog.ActiveText = "Yes";
            this.tbSaveLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.tbSaveLog.InActiveColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.tbSaveLog.InActiveText = "No";
            this.tbSaveLog.Location = new System.Drawing.Point(245, 535);
            this.tbSaveLog.MaximumSize = new System.Drawing.Size(119, 32);
            this.tbSaveLog.MinimumSize = new System.Drawing.Size(75, 23);
            this.tbSaveLog.Name = "tbSaveLog";
            this.tbSaveLog.Size = new System.Drawing.Size(75, 23);
            this.tbSaveLog.SliderColor = System.Drawing.Color.Black;
            this.tbSaveLog.SlidingAngle = 8;
            this.tbSaveLog.TabIndex = 20;
            this.tbSaveLog.TextColor = System.Drawing.Color.White;
            this.tbSaveLog.ToggleState = MyCustomControl.ToggleButton.ToggleButtonState.ON;
            this.tbSaveLog.ToggleStyle = MyCustomControl.ToggleButton.ToggleButtonStyle.Android;
            // 
            // myTreeView
            // 
            this.myTreeView.BackColor = System.Drawing.SystemColors.Control;
            this.myTreeView.HideSelection = false;
            this.myTreeView.ImageIndex = 0;
            this.myTreeView.ImageList = this.TreeNodeIcons;
            this.myTreeView.Location = new System.Drawing.Point(12, 36);
            this.myTreeView.Name = "myTreeView";
            this.myTreeView.SelectedImageIndex = 0;
            this.myTreeView.Size = new System.Drawing.Size(210, 460);
            this.myTreeView.TabIndex = 0;
            this.myTreeView.BeforeCheck += new System.Windows.Forms.TreeViewCancelEventHandler(this.myTreeView_BeforeCheck);
            this.myTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.myTreeView_AfterCheck);
            this.myTreeView.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this.myTreeView_BeforeCollapse);
            this.myTreeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.myTreeView_BeforeExpand);
            this.myTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.myTreeView_AfterSelect);
            this.myTreeView.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.myTreeView_NodeMouseDoubleClick);
            // 
            // TestServerGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(1097, 596);
            this.Controls.Add(this.lblSaveLog);
            this.Controls.Add(this.tbSaveLog);
            this.Controls.Add(this.myTreeView);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.logTxtBox);
            this.Controls.Add(this.StopServerButton);
            this.Controls.Add(this.StartServerButton);
            this.Controls.Add(this.startTestButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblDesc);
            this.Controls.Add(this.txtBoxDes);
            this.Controls.Add(this.tcDataGridView);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "TestServerGUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automatic Test Suite";
            this.Activated += new System.EventHandler(this.TestServerGUI_Activated);
            this.Deactivate += new System.EventHandler(this.TestServerGUI_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.tcDataGridView)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView tcDataGridView;
        private System.Windows.Forms.TextBox txtBoxDes;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem testSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem continueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem portConnectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enterNewPortToolStripMenuItem;
        private System.Windows.Forms.Button startTestButton;
        private System.Windows.Forms.ToolStripMenuItem portToolStripMenuItem;
        private System.Windows.Forms.Button StartServerButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblFailTest;
        private System.Windows.Forms.Label lblPassTest;
        private System.Windows.Forms.Label lblTotalTest;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem openXMLFromFolderToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker keepTrackHostClient;
        private System.Windows.Forms.Button StopServerButton;
        private System.Windows.Forms.ImageList TreeNodeIcons;
        private System.ComponentModel.BackgroundWorker RunningTest;
        private System.Windows.Forms.TextBox logTxtBox;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel IPStatuslbl;
        private System.Windows.Forms.ToolStripStatusLabel PortStatuslbl;
        private System.Windows.Forms.ToolStripStatusLabel serverStatuslbl;
        private System.Windows.Forms.ToolStripStatusLabel testSettingStatuslbl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox manualchkBox;
        private System.Windows.Forms.CheckBox autochkBox;
        private MyCustomControl.MyTreeView myTreeView;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.DataGridViewTextBoxColumn SeqNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiagnosticCmd;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Parameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpOutcome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Response;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteDatabseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeTimeoutDurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private MyCustomControl.ToggleButton tbSaveLog;
        private System.Windows.Forms.Label lblSaveLog;
    }
}

