/*
  Filename  : Diag.c
*/

/* Standard Library */
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
/* Kernel */
#include "Typedefs.h"
#include "Kernel.h"
#include "UserInput.h"
#include "UserInterface.h"
#include "Log.h"
/* Driver */
#include "Ethernet.h"
#include "Keypad.h"
/* Diag */
#include "DiagRespCodes.h"
/* Header File */
#include "Diag.h"

/* Log Defines */
#ifdef LOG_SHORTNAME
#undef LOG_SHORTNAME
#endif
#define LOG_SHORTNAME						"DAPP"

/* Private Variables */

/* Private Function Prototypes */

/* Private Functions */

/* Public Functions */
UInt8_t DiagApp_Execute( Char_t iCommand[], Char_t iParam[], Char_t oResult[] )
{
    return DIAG_RC_ERROR;
}

/* EOF */
