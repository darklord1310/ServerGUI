/*
  Filename  : Diag.c
*/

/* Standard Library */
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
/* Kernel */
#include "Typedefs.h"
#include "Kernel.h"
#include "UserInput.h"
#include "UserInterface.h"
#include "Log.h"
/* Driver */
#include "Ethernet.h"
#include "Keypad.h"
#include "Printer.h"
#include "Contactless.h"
#include "SmartCard.h"
/* Library */
#include "Library.h"
/* Diag */
#include "DiagRespCodes.h"
/* Header File */
#include "Diag.h"

/* Log Defines */
#ifdef LOG_SHORTNAME
#undef LOG_SHORTNAME
#endif
#define LOG_SHORTNAME						"DIAG"

/* State Machine */
#define DIAG_SET_STM_ENTRY					1
#define DIAG_SET_STM_SET_HOST_IP			2
#define DIAG_SET_STM_PROMPT_HOST_PORT		3
#define DIAG_SET_STM_SET_HOST_PORT			4
#define DIAG_SET_STM_EXIT					5

#define DIAG_CONN_STM_ENTRY					1
#define DIAG_CONN_STM_SET_DHCP				2
#define DIAG_CONN_STM_CONNECT				3
#define DIAG_CONN_STM_CONNECTING			4
#define DIAG_CONN_STM_WAIT_CMD				5
#define DIAG_CONN_STM_DISCONNECT			6
#define DIAG_CONN_STM_EXIT					7

/* Externs */
extern UInt8_t DiagDrv_Execute( Char_t iCommand[], Char_t iParam[], Char_t oResult[] );
extern UInt8_t DiagLib_Execute( Char_t iCommand[], Char_t iParam[], Char_t oResult[] );
extern UInt8_t DiagApp_Execute( Char_t iCommand[], Char_t iParam[], Char_t oResult[] );

/* Private Variables */
Char_t      szDiagHostIP_[16];
Char_t      szDiagHostPort_[6];
UInt16_t    sDiagState_;
UInt16_t    sDiagIdleTimerId_;
UInt16_t    sDiagTimerId_;
UInt16_t    sDiagPollTimerId_;
UInt16_t    sDiagRecvOffset_;

/* Private Function Prototypes */
void   Diag_MenuOnKeyPressed( void );
void   Diag_SetupSTM( void );
void   Diag_ConnectSTM( void );
void   Diag_ConnectTimeout( void );
void   Diag_OnKeyPressed( void );
void   Diag_Execute( Char_t iCommand[], UInt16_t iCmdLength, Char_t oResponse[], UInt16_t *oRespLength );
void   Diag_CardDetect( void );

/* Private Functions */
void Diag_MenuOnKeyPressed( void )
{
	UInt8_t	    bKey;

	bKey = Kernel_GetKeyPressed();
	switch( bKey )
	{
        case KEYP_KEY_1:
            sDiagState_ = DIAG_SET_STM_ENTRY;
			Diag_SetupSTM();
            break;

        case KEYP_KEY_2:
			sDiagState_ = DIAG_CONN_STM_ENTRY;
			Diag_ConnectSTM();
            break;
            
        case KEYP_KEY_F1:
			Kernel_PowerOff();
            break;

        case KEYP_KEY_F3:
            CL_Open( );
			Kernel_StartTimer( sDiagIdleTimerId_ );
            break;

        case KEYP_KEY_F4:
            Kernel_StopTimer( sDiagIdleTimerId_ );
            CL_Close( );
            
            UI_ClearScreen();
            UI_ShowText( 1, TEXT_ALIGN_CENTER, TEXT_STYLE_TITLE, "DIAGNOSTIC MENU" );
            UI_ShowText( 2, TEXT_ALIGN_LEFT, 0, "1 Setup" );
            UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "2 Connect" );
            UI_ShowText( 8, TEXT_ALIGN_LEFT, 0, "[F1] Exit" );
            break;

        default:
            break;
	}
}

void Diag_SetupSTM( void )
{
    Char_t      szBuffer[41];
    UInt8_t     bInputResult;

    if( DIAG_SET_STM_ENTRY == sDiagState_ )
    {
        UI_ClearScreen();
        UI_ShowText( 1, TEXT_ALIGN_CENTER, TEXT_STYLE_TITLE, "DIAG SETUP" );
        UI_ShowText( 2, TEXT_ALIGN_LEFT, 0, "Host IP:" );
        sDiagState_ = DIAG_SET_STM_SET_HOST_IP;

        UInput_Init( UINP_TYP_IP, 3, TEXT_ALIGN_LEFT, 0, 15 );
        if( 0 < strlen( szDiagHostIP_ ) )
        {
            UInput_SetDefaultText( szDiagHostIP_ );
        }
        UInput_Start( Diag_SetupSTM, 60000 );
        return;
    }
    if( DIAG_SET_STM_SET_HOST_IP == sDiagState_ )
    {
        bInputResult = UInput_GetText( szBuffer );
        if( UINP_RC_SUCCESS == bInputResult )
        {
            strcpy( szDiagHostIP_, szBuffer );
            sDiagState_ = DIAG_SET_STM_PROMPT_HOST_PORT;
        }
        else
        {
            sDiagState_ = DIAG_SET_STM_EXIT;
        }
    }
    if( DIAG_SET_STM_PROMPT_HOST_PORT == sDiagState_ )
    {
        UI_ShowText( 5, TEXT_ALIGN_LEFT, 0, "Host Port:" );
        sDiagState_ = DIAG_SET_STM_SET_HOST_PORT;

        UInput_Init( UINP_TYP_NUMERIC, 6, TEXT_ALIGN_LEFT, 0, 5 );
        if( 0 < strlen( szDiagHostPort_ ) )
        {
            UInput_SetDefaultText( szDiagHostPort_ );
        }
        UInput_Start( Diag_SetupSTM, 60000 );
        return;
    }
    if( DIAG_SET_STM_SET_HOST_PORT == sDiagState_ )
    {
        bInputResult = UInput_GetText( szBuffer );
        if( UINP_RC_SUCCESS == bInputResult )
        {
            strcpy( szDiagHostPort_, szBuffer );
        }
        sDiagState_ = DIAG_SET_STM_EXIT;
    }
    if( DIAG_SET_STM_EXIT == sDiagState_ )
    {
        sDiagState_ = 0;
        Kernel_DelayExecute( 5, DiagMenu_Show );
    }
}

void Diag_ConnectSTM( void )
{
	Bool_t		isEnable;
	UInt8_t		baSendBuffer[2048];
	UInt8_t		baRecvBuffer[2048];
	UInt16_t	sSendLength;
	UInt16_t	sRecvLength;
    UInt16_t	sLoop;
	UInt16_t	sStatus;

    if( DIAG_CONN_STM_ENTRY == sDiagState_ )
    {
        sDiagTimerId_ = 0;
        sDiagPollTimerId_ = 0;

        UI_ClearScreen();
        UI_ShowText( 1, TEXT_ALIGN_CENTER, TEXT_STYLE_TITLE, "DIAG CONNECT" );

        if( ( 0 == strlen( szDiagHostIP_ ) ) || ( 0 == strlen( szDiagHostPort_ ) ) )
        {
            UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "Please run setup" );
            UI_ShowText( 4, TEXT_ALIGN_LEFT, 0, "first!" );
            sDiagState_ = DIAG_CONN_STM_EXIT;
        }
        else
        {
            if( TRUE == Eth_Open() )
            {
                sDiagState_ = DIAG_CONN_STM_SET_DHCP;

            }
            else
            {
                UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "Open failed!" );
                sDiagState_ = DIAG_CONN_STM_EXIT;
            }
        }
    }
    if( DIAG_CONN_STM_SET_DHCP == sDiagState_ )
    {
        isEnable = FALSE;
        if( FALSE == Eth_GetDHCP( &isEnable ) )
        {
            isEnable = FALSE;
        }
        if( FALSE == isEnable )
        {
            if( TRUE == Eth_SetDHCP( TRUE ) )
            {
                isEnable = TRUE;
            }
        }
        if( TRUE == isEnable )
        {
            UI_ShowText( 2, TEXT_ALIGN_LEFT, 0, "Connecting..." );
            sDiagState_ = DIAG_CONN_STM_CONNECT;
            Kernel_RegisterTimer( 60000, FALSE, Diag_ConnectTimeout, &sDiagTimerId_ );
            Kernel_RegisterTimer( 200, TRUE, Diag_ConnectSTM, &sDiagPollTimerId_ );
        }
        else
        {
            UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "Set DHCP failed!" );
            Eth_Close();
            sDiagState_ = DIAG_CONN_STM_EXIT;
        }
    }
    if( DIAG_CONN_STM_CONNECT == sDiagState_ )
    {
        sStatus = Eth_GetStatus();
        if( ETH_STATUS_PHY_LINK & sStatus )
        {
            if( TRUE == Eth_Connect( szDiagHostIP_, szDiagHostPort_ ) )
            {
                sDiagState_ = DIAG_CONN_STM_CONNECTING;
                Kernel_ResetTimer( sDiagTimerId_ );
                Kernel_ResetTimer( sDiagPollTimerId_ );
            }
            else
            {
                UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "Connect failed!" );
                Eth_Close();
                sDiagState_ = DIAG_CONN_STM_EXIT;
            }
        }
    }
    if( DIAG_CONN_STM_CONNECTING == sDiagState_ )
    {
        sStatus = Eth_GetStatus();
        if( ETH_STATUS_ONLINE & sStatus )
        {
            Kernel_UnregisterTimer( sDiagTimerId_ );

            UI_ClearScreen();
            UI_ShowText( 1, TEXT_ALIGN_CENTER, TEXT_STYLE_TITLE, "DIAG CONNECT" );
            UI_ShowText( 2, TEXT_ALIGN_LEFT, 0, "Connected." );
            UI_ShowText( 4, TEXT_ALIGN_LEFT, 0, "Wait for command..." );
            sDiagState_ = DIAG_CONN_STM_WAIT_CMD;
            sDiagRecvOffset_ = 0;
            Kernel_RegisterEvent( EVT_KEY_PRESSED, Diag_OnKeyPressed );
            Kernel_ConfigTimer( sDiagPollTimerId_, 50, TRUE );
            Kernel_ResetTimer( sDiagPollTimerId_ );
        }
    }
    if( DIAG_CONN_STM_WAIT_CMD == sDiagState_ )
    {
        sRecvLength = 2048 - sDiagRecvOffset_;
        if( FALSE == Eth_Receive( &baRecvBuffer[sDiagRecvOffset_], &sRecvLength ) )
        {
            return;
        }
        if( 0 == sRecvLength ) 
        {
            return;
        }
        sDiagRecvOffset_ += sRecvLength;
        sRecvLength = 0;
        for( sLoop=0; sLoop<sDiagRecvOffset_; sLoop++ )
        {
            if( '\n' == baRecvBuffer[sLoop] )
            {
                sRecvLength = sLoop;
                baRecvBuffer[sLoop] = 0;
                break;
            }
        }
        if( 0 == sRecvLength )
        {
            return;
        }
        sDiagRecvOffset_ = 0;
        
        UI_ClearScreen();
        UI_ShowText( 1, TEXT_ALIGN_CENTER, TEXT_STYLE_TITLE, "DIAG CONNECT" );
        UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "Processing..." );
        UI_ShowText( 4, TEXT_ALIGN_LEFT, 0, baRecvBuffer );
        
        Diag_Execute( baRecvBuffer, sRecvLength, baSendBuffer, &sSendLength );
        if( 0 < sSendLength )
        {
            if( FALSE == Eth_Send( baSendBuffer, sSendLength ) )
            {
                UI_ClearScreen();
                UI_ShowText( 1, TEXT_ALIGN_CENTER, TEXT_STYLE_TITLE, "DIAG CONNECT" );
                UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "Send response" );
                UI_ShowText( 4, TEXT_ALIGN_LEFT, 0, "failed!" );
                Eth_Disconnect();
                Eth_Close();
                sDiagState_ = DIAG_CONN_STM_EXIT;
            }
        }
        
    }
    if( DIAG_CONN_STM_DISCONNECT == sDiagState_ )
    {
        UI_ClearScreen();
        UI_ShowText( 1, TEXT_ALIGN_CENTER, TEXT_STYLE_TITLE, "DIAG CONNECT" );
        UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "Disconnecting..." );
        Eth_Disconnect();
        Eth_Close();
        sDiagState_ = DIAG_CONN_STM_EXIT;
    }
    if( DIAG_CONN_STM_EXIT == sDiagState_ )
    {
        Kernel_UnregisterEvent( EVT_KEY_PRESSED );
        Kernel_UnregisterTimer( sDiagTimerId_ );
        Kernel_UnregisterTimer( sDiagPollTimerId_ );
        sDiagState_ = 0;
        Kernel_DelayExecute( 3000, DiagMenu_Show );
	}
}

void Diag_ConnectTimeout( void )
{
	UI_ClearScreen();
	UI_ShowText( 1, TEXT_ALIGN_CENTER, TEXT_STYLE_TITLE, "DIAG CONNECT" );
	UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "Connect timeout!" );
	sDiagState_ = DIAG_CONN_STM_EXIT;
	Diag_ConnectSTM();
}

void Diag_OnKeyPressed( void )
{
	if( KEYP_KEY_CANCEL == Kernel_GetKeyPressed() )
	{
		sDiagState_ = DIAG_CONN_STM_DISCONNECT;
		Diag_ConnectSTM();
	}
}

void Diag_Execute( Char_t iCommand[], UInt16_t iCmdLength, Char_t oResponse[], UInt16_t *oRespLength )
{
    Char_t      szCommand[21];
    Char_t      szParam[1024];
    Char_t      szResult[1024];
    UInt8_t     bStatus;
    UInt16_t    sCmdLength;
    UInt16_t    sRespLength;
    UInt16_t    sLength;

    memset( szCommand, 0, sizeof(szCommand) );
    memset( szParam, 0, sizeof(szParam) );
    memset( szResult, 0, sizeof(szResult) );

    sCmdLength = strcspn( iCommand, ":" );
    if( ( 4 < iCmdLength ) && ( 20 >= sCmdLength ) )
    {
        memcpy( szCommand, iCommand, sCmdLength );
        if( iCmdLength > ( sCmdLength + 1 ) )
        {
            sLength = iCmdLength - ( sCmdLength + 1 );
            memcpy( szParam, &iCommand[sCmdLength + 1], sLength );
        }
        switch( szCommand[0] ) /* Category ID */
        {
//          case 'K': /* Kernel */
//              break;

            case 'D': /* Driver */
                bStatus = DiagDrv_Execute( szCommand, szParam, szResult );
                break;

            case 'L': /* Library */
                bStatus = DiagLib_Execute( szCommand, szParam, szResult );
                break;

            case 'A': /* App */
                bStatus = DiagApp_Execute( szCommand, szParam, szResult );
                break;

    //		case 'V': /* View */
    //			break;

    //		case 'T': /* TNG */
    //			break;

    //		case 'E': /* EDB */
    //			break;

            default:
                LOG_EXCEPTION();
                bStatus = DIAG_RC_INVALID_CMD;
                break;
        }
    }
    else
    {
        LOG_EXCEPTION();
        bStatus = DIAG_RC_INVALID_CMD;
    }

    memcpy( oResponse, iCommand, sCmdLength );
    sRespLength = sCmdLength;
    sprintf( &oResponse[sRespLength], "=%02d", bStatus );
    sRespLength += 3;
    sLength = strlen( szResult );
    if( 0 < sLength )
    {
        oResponse[sRespLength++] = ':';
        memcpy( &oResponse[sRespLength], szResult, sLength );
        sRespLength += sLength;
    }
    oResponse[sRespLength++] = 0x0A;
    oResponse[sRespLength] = 0;
    *oRespLength = sRespLength;
}

void Diag_CardDetect( void )
{
    Bool_t  isSuccess;
    UInt8_t baCardSN[16];
    UInt8_t bCardSNLength;
    UInt8_t baATQA[2];
    UInt8_t bSAK;
    UInt8_t baBuffer[64];
    UInt8_t baBuffer2[64];
    Char_t  szBuffer[41];
    Char_t  szBuffer2[41];
    
    isSuccess = CL_ActiveIdleTypeA( baCardSN, &bCardSNLength, baATQA, &bSAK );
    if( TRUE == isSuccess )
    {
        UI_ClearScreen();
        UI_ShowText( 1, TEXT_ALIGN_LEFT, 0, "Card detected." );
        isSuccess = CL_MiFareClassicLogin( 0, CL_MFC_KEY_TYPE_A, "\xA0\xA1\xA2\xA3\xA4\xA5" );
        if( TRUE == isSuccess )
        {
            UI_ShowText( 2, TEXT_ALIGN_LEFT, 0, "Card login success." );
            //HexArrayToString( baCardSN, 10, szBuffer2 );
            //UI_ShowText( 6, TEXT_ALIGN_LEFT, 0, szBuffer2 );
            //HexArrayToString( &baCardSN[10], 6, szBuffer2 );
            //UI_ShowText( 7, TEXT_ALIGN_LEFT, 0, szBuffer2 );
            HexArrayToString( baATQA, 2, szBuffer2 );
            UI_ShowText( 6, TEXT_ALIGN_LEFT, 0, szBuffer2 );
            sprintf(szBuffer2, "%02x", bSAK);
            UI_ShowText( 7, TEXT_ALIGN_LEFT, 0, szBuffer2 );
        }
        else
        {
            UI_ShowText( 2, TEXT_ALIGN_LEFT, 0, "Card login failed." );
        }
        isSuccess = CL_MiFareClassicRead( 0, baBuffer );
        if( TRUE == isSuccess )
        {
            UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "Card read success." );
            HexArrayToString( baBuffer, 10, szBuffer );
            UI_ShowText( 4, TEXT_ALIGN_LEFT, 0, szBuffer );
            HexArrayToString( &baBuffer[10], 6, szBuffer );
            UI_ShowText( 5, TEXT_ALIGN_LEFT, 0, szBuffer );
        }
        else
        {
            UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "Card read failed." );
        }
    }
}

/* Public Functions */
void DiagMenu_Show( void )
{
///
//    strcpy( szDiagHostIP_, "10.193.0.174" );
    strcpy( szDiagHostPort_, "25000" );
///    
    
    UI_ClearScreen();
    UI_ShowText( 1, TEXT_ALIGN_CENTER, TEXT_STYLE_TITLE, "DIAGNOSTIC MENU" );
    UI_ShowText( 2, TEXT_ALIGN_LEFT, 0, "1 Setup" );
    UI_ShowText( 3, TEXT_ALIGN_LEFT, 0, "2 Connect" );
    UI_ShowText( 8, TEXT_ALIGN_LEFT, 0, "[F1] Exit" );
    Kernel_RegisterEvent( EVT_KEY_PRESSED, Diag_MenuOnKeyPressed );
    Kernel_RegisterTimer( 1000, TRUE, Diag_CardDetect, &sDiagIdleTimerId_ );
}

void DiagMenu_Close( void )
{
    Kernel_UnregisterEvent( EVT_KEY_PRESSED );
    Kernel_UnregisterTimer( sDiagIdleTimerId_ );
}

/* EOF */
