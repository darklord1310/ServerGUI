/*
  Filename  : Diag.h
*/

#ifndef DIAG_H_
#define DIAG_H_

#ifdef __cplusplus
extern "C"
{
#endif

/* Public Function Prototypes */

void		DiagMenu_Show( void );

void		DiagMenu_Close( void );

#ifdef __cplusplus
}
#endif

#endif  /* DIAG_H_ */
