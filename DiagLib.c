/*
  Filename  : Diag.c
*/

/* Standard Library */
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
/* Kernel */
#include "Typedefs.h"
#include "Kernel.h"
#include "UserInput.h"
#include "UserInterface.h"
#include "Log.h"
/* Driver */
#include "Ethernet.h"
#include "Keypad.h"
/* Library */
#include "AtomicFile.h"
#include "Checksum.h"
#include "CtlsManager.h"
#include "Library.h"
#include "DateTime.h"
#include "FileList.h"
#include "TLV.h"
#include "RecFile.h"
#include "ImagePBM.h"
#include "Hash.h"
#include "SamManager.h"
/* Diag */
#include "DiagRespCodes.h"
/* Header File */
#include "Diag.h"

/* Log Defines */
#ifdef LOG_SHORTNAME
#undef LOG_SHORTNAME
#endif
#define LOG_SHORTNAME						"DLIB"

/* Private Variables */
DateTime_t hDiagDateTime;
CardInfoCL_t hDiagCardInfoCL;
APDU_t hDiagAPDU;
TLV_t hDiagTLV;
RecFile_t hDiagRecFile;
SHA1Ctx_t context;
    
/* Private Function Prototypes */
UInt8_t DiagLib_AtomicFile( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagLib_Checksum( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagLib_CrlsManager( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagLib_DateTime( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagLib_FileList( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagLib_TLV( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagLib_Library( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagLib_RecFile( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagLib_ImagePBM( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagLib_Hash( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );
UInt8_t DiagLib_SamManager( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] );

/* Private Functions */
UInt8_t DiagLib_AtomicFile( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t isResult;
    Char_t *pszParam;
    if( 0 == strcmp( "START", iFuncName ) )
    {
      AtomicFile_Start();
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "COMMIT", iFuncName ) )
    {
      AtomicFile_Commit();
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "ROLLBACK", iFuncName ) )
    {
      AtomicFile_Rollback();
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "INCLUDE", iFuncName ) )
    {
        pszParam = strtok( iParam, "|" );
        if( pszParam == NULL)
           return DIAG_RC_INVALID_PARAM;

        isResult = AtomicFile_Include(pszParam);
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else
        return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagLib_Checksum( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    UInt16_t Value16;
    UInt32_t Value32;
    UInt8_t temp[1024];
    UInt8_t baBuffer[1024];
    UInt16_t checksum16;
    UInt32_t checksum32;
    UInt32_t sLength;    
    Char_t *pszParam, *pszParam2;
    
    if( 0 == strcmp( "CRC16", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      pszParam2 = strtok( NULL, "|" );
      if( pszParam == NULL || pszParam2 == NULL)
           return DIAG_RC_INVALID_PARAM;
      if( strlen(pszParam) != 4)
           return DIAG_RC_INVALID_PARAM;
      StringToHexArray( pszParam, strlen(pszParam), temp );
      checksum16 = GetUInt16( temp );
      sLength = StringToHexArray( pszParam2, strlen(pszParam2), baBuffer );
      Value16 = CRC16_GetChecksum( checksum16, baBuffer ,sLength );
      sprintf(oResult, "%04x", Value16);
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CRC32", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      pszParam2 = strtok( NULL, "|" );
      if( pszParam == NULL || pszParam2 == NULL)
           return DIAG_RC_INVALID_PARAM;

      StringToHexArray( pszParam, strlen(pszParam), temp );
      checksum32 = GetUInt32( temp );
      sLength = StringToHexArray( pszParam2, strlen(pszParam2), baBuffer );
      Value32 = CRC32_GetChecksum( checksum32, baBuffer ,sLength );
      sprintf(oResult, "%0lx", Value32);
      return DIAG_RC_SUCCESS;
    }
    else
        return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagLib_CrlsManager( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    UInt8_t     bValue1;
    UInt8_t     bValue2;
    UInt8_t     baBuffer[1024];
    UInt8_t     baBuffer2[4];
    UInt16_t     sLength;
    UInt32_t    lValue;
    Bool_t      isResult;
    Char_t      *pszParam1;
    Char_t      *pszParam2;
    Char_t      *pszParam3;
    Char_t      *pszParam4;
    
    if( 0 == strcmp( "OPEN", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = CLM_Open( (UInt8_t)StringToUInt32( iParam, sLength ) );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLOSE", iFuncName ) )
    {
        CLM_Close();
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETID", iFuncName ) )
    {
        isResult = CLM_GetReaderID( &oResult[2] );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETBL", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = CLM_SetBacklight( iParam[0] - '0' );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETBUZZ", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = CLM_SetBuzzer( (UInt16_t)StringToUInt32( iParam, sLength ) );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETDISP", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || \
            ( NULL == pszParam3 ) || ( NULL == pszParam4 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = CLM_SetDisplay( pszParam1, pszParam2, pszParam3, pszParam4 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETLED", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        pszParam4 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || \
            ( NULL == pszParam3 ) || ( NULL == pszParam4 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = CLM_SetLED( (pszParam1[0] - '0'), (pszParam2[0] - '0'), \
                               (pszParam3[0] - '0'), (pszParam4[0] - '0') );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETRF", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( 0 == sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = CLM_SetRFPower( iParam[0] - '0' );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CARDDETECT", iFuncName ) )
    {
        isResult = CLM_IsCardDetected();
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETCARD", iFuncName ) )
    {
        isResult = CLM_GetCard( &hDiagCardInfoCL );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString( hDiagCardInfoCL.m_UID, hDiagCardInfoCL.m_UIDLength, baBuffer );
        HexArrayToString( hDiagCardInfoCL.m_ATQA, 2, baBuffer2 );
        sLength = sprintf( &oResult[2], "%s,%x,%x,%s,%x", baBuffer,                    \
                                                          hDiagCardInfoCL.m_UIDLength, \
                                                          hDiagCardInfoCL.m_CardType,  \
                                                          baBuffer2,                   \
                                                          hDiagCardInfoCL.m_SAK );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCLOGIN", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        pszParam3 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( pszParam1, sLength );
        
        if( 2 != strlen( pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( pszParam2, strlen(pszParam2), baBuffer );
        bValue2 = (UInt8_t)GetUInt16( baBuffer );
        
        sLength = strlen( pszParam3 );
        if( 12 != sLength )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        StringToHexArray( pszParam3, sLength, baBuffer2 );
        isResult = CLM_MiFareLogin( bValue1, bValue2, baBuffer2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCREAD", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( iParam, sLength );
        isResult = CLM_MiFareRead( bValue1, baBuffer );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString( baBuffer, strlen( baBuffer ), &oResult[2] );  /* Error might occur, if Hex Array contain 0 in the middle */
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCWRITE", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( pszParam1, sLength );
        StringToHexArray( pszParam2, strlen( pszParam2 ), baBuffer );
        isResult = CLM_MiFareWrite( bValue1, baBuffer );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCINC", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( pszParam1, sLength );
        
        sLength = strlen( pszParam2 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = StringToUInt32( pszParam2, sLength );
        isResult = CLM_MiFareIncrement( bValue1, lValue );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCDEC", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( pszParam1, sLength );
        
        sLength = strlen( pszParam2 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = StringToUInt32( pszParam2, sLength );
        isResult = CLM_MiFareDecrement( bValue1, lValue );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "MFCTRS", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue1 = (UInt8_t)StringToUInt32( pszParam1, sLength );
        
        sLength = strlen( pszParam2 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue2 = (UInt8_t)StringToUInt32( pszParam2, sLength );
        isResult = CLM_MiFareTransfer( bValue1, bValue2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagLib_DateTime( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t      isResult;
    UInt8_t     baBuffer[1024];
    UInt16_t    sLength;
    UInt32_t    lValue;
    Char_t      *pszParam1;
    Char_t      *pszParam2;
    Char_t      *pszParam3;
    Char_t      *pszParam4;
    Char_t      *pszParam5;
    Char_t      *pszParam6;
    
    if( 0 == strcmp( "GETALL", iFuncName ) )
    {
        lValue = DateTime_Get();
        sprintf( oResult, "%lu", lValue );
        //SetUInt32( lValue, baBuffer );
        //HexArrayToString( baBuffer, strlen( baBuffer ), oResult );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETDATE", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = DateTime_GetDate( StringToUInt32( iParam, sLength ) );
        sprintf( oResult, "%lu", lValue );
        //SetUInt32( lValue, baBuffer );
        //HexArrayToString( baBuffer, strlen( baBuffer ), oResult );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETTIME", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = DateTime_GetTime( StringToUInt32( iParam, sLength ) );
        sprintf( oResult, "%lu", lValue );
        //SetUInt32( lValue, baBuffer );
        //HexArrayToString( baBuffer, strlen( baBuffer ), oResult );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SET", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = DateTime_Set( StringToUInt32( iParam, sLength ) );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "ENCODE", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "," );
        pszParam2 = strtok( NULL, "," );
        pszParam3 = strtok( NULL, "," );
        pszParam4 = strtok( NULL, "," );
        pszParam5 = strtok( NULL, "," );
        pszParam6 = strtok( NULL, "," );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) || ( NULL == pszParam3 ) || \
            ( NULL == pszParam4 ) || ( NULL == pszParam5 ) || ( NULL == pszParam6 ))
        {
            return DIAG_RC_INVALID_PARAM;
        }
        hDiagDateTime.m_Year = ( UInt16_t )StringToUInt32( pszParam1, strlen( pszParam1 ) );
        hDiagDateTime.m_Month = ( UInt8_t )StringToUInt32( pszParam2, strlen( pszParam2 ) );
        hDiagDateTime.m_Day = ( UInt8_t )StringToUInt32( pszParam3, strlen( pszParam3 ) );
        hDiagDateTime.m_Hour = ( UInt8_t )StringToUInt32( pszParam4, strlen( pszParam4 ) );
        hDiagDateTime.m_Minute = ( UInt8_t )StringToUInt32( pszParam5, strlen( pszParam5 ) );
        hDiagDateTime.m_Second = ( UInt8_t )StringToUInt32( pszParam6, strlen( pszParam6 ) );
        lValue = DateTime_Encode( &hDiagDateTime );
        sprintf( oResult, "%lu", lValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DECODE", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        DateTime_Decode( StringToUInt32( iParam, sLength ), &hDiagDateTime );
        sLength = sprintf( oResult, "%d,%d,%d,%d,%d,%d", hDiagDateTime.m_Year,  \
                                                         hDiagDateTime.m_Month, \
                                                         hDiagDateTime.m_Day,   \
                                                         hDiagDateTime.m_Hour,  \
                                                         hDiagDateTime.m_Minute,\
                                                         hDiagDateTime.m_Second );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "FORMAT", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        lValue = StringToUInt32( pszParam1, sLength );
        DateTime_Format( lValue, pszParam2, oResult );
        return DIAG_RC_SUCCESS;
    }
    return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagLib_FileList( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t      isResult;
    Char_t      *pszParam1;
    Char_t      *pszParam2;
    
    if( 0 == strcmp( "INIT", iFuncName ) )
    {
        FileList_Init( iParam );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "ADD", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        isResult = FileList_AddFile( pszParam1, pszParam2 );
        oResult[0] = isResult + '0';
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "REMOVE", iFuncName ) )
    {
        FileList_RemoveAllFiles( iParam );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "BACKUP", iFuncName ) )
    {
        FileList_Backup( iParam );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "RESTORE", iFuncName ) )
    {
        FileList_Restore( iParam );
        return DIAG_RC_SUCCESS;
    }
    return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagLib_TLV( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t isResult;
    UInt8_t Value8,temp2;
    UInt16_t Value16;
    UInt8_t temp[1024];
    UInt8_t baBuffer[1024];
    UInt32_t sLength;    
    Char_t *pszParam, *pszParam2, *pszParam3, tempStr[1024];
    
    if( 0 == strcmp( "INIT", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      pszParam2 = strtok( NULL, "|" );
      pszParam3 = strtok( NULL, "|" );
      if( pszParam == NULL || pszParam2 == NULL || pszParam3 == NULL)
           return DIAG_RC_INVALID_PARAM;
      StringToHexArray(pszParam, strlen(pszParam), baBuffer);
      TLV_Init( baBuffer, StringToUInt32( pszParam2, strlen(pszParam2)), StringToUInt32( pszParam3, strlen(pszParam3)), &hDiagTLV );
      
      HexArrayToString( hDiagTLV.m_DataBuffer, hDiagTLV.m_DataLength, tempStr );
      sprintf(oResult, "%s,%d,%d", tempStr, hDiagTLV.m_BufferSize, hDiagTLV.m_DataLength);
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "APPENDOBJECT", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      pszParam2 = strtok( NULL, "|" );
      if( pszParam == NULL || pszParam2 == NULL)
           return DIAG_RC_INVALID_PARAM;
      if( strlen(pszParam) != 2)
           return DIAG_RC_INVALID_PARAM;
      StringToHexArray( pszParam, strlen(pszParam), temp );
      Value8 = (UInt8_t)GetUInt16( temp );
      sLength = StringToHexArray( pszParam2, strlen(pszParam2), baBuffer );
      TLV_AppendObject(&hDiagTLV, Value8, sLength, baBuffer);
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "APPENDEXTLENGTH", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      pszParam2 = strtok( NULL, "|" );
      if( pszParam == NULL || pszParam2 == NULL)
           return DIAG_RC_INVALID_PARAM;
      if( strlen(pszParam) != 2)
           return DIAG_RC_INVALID_PARAM;
      StringToHexArray( pszParam, strlen(pszParam), temp );
      Value8 = (UInt8_t)GetUInt16( temp );
      sLength = StringToHexArray( pszParam2, strlen(pszParam2), baBuffer );
      TLV_AppendObjectExtLength(&hDiagTLV, Value8, sLength, baBuffer );
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETLENGTH", iFuncName ) )
    {
      Value16 = TLV_GetDataLength(&hDiagTLV);
      sprintf(oResult, "%d", Value16);
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETOBJECT", iFuncName ) )
    {
      isResult = TLV_GetObject( &hDiagTLV, &Value8, &temp2, baBuffer );
      oResult[0] = isResult + '0';
      oResult[1] = '|';
      sprintf( &oResult[2], "%02X|", Value8 );
      HexArrayToString( baBuffer, temp2 , &oResult[4] );
      return DIAG_RC_SUCCESS;
    }
    else
        return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagLib_Library( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    UInt16_t Value16;
    UInt32_t Value, sLength;
    Char_t *pszParam;
    Char_t strReturn[1024];
    UInt8_t baBuffer[1024];
    UInt8_t baBuffer2[1024] = "";
    UInt8_t temp[1024];
    
    if( 0 == strcmp( "GETUINT16", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL)
           return DIAG_RC_INVALID_PARAM;
      StringToHexArray( pszParam, strlen(pszParam), baBuffer);
      Value16 = GetUInt16(baBuffer);
      sprintf(oResult, "%04x", Value16);
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETUINT16", iFuncName ) )
    {
      sLength = strlen( iParam );
      if( sLength != 4)
           return DIAG_RC_INVALID_PARAM;
      sLength = StringToHexArray( iParam, sLength, baBuffer );
      Value16 = GetUInt16( baBuffer );
      //Value16 = (UInt16_t)StringToUInt32( pszParam, strlen(pszParam) );
      SetUInt16( Value16, baBuffer);
      HexArrayToString( baBuffer, sLength, oResult );
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETUINT24", iFuncName ) )
    { 
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL)
           return DIAG_RC_INVALID_PARAM;
      StringToHexArray( pszParam, strlen(pszParam), baBuffer);
      Value = GetUInt24(baBuffer);
      sprintf(oResult, "%06lx", Value);
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETUINT24", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( sLength != 6)
           return DIAG_RC_INVALID_PARAM;
        sLength = StringToHexArray( iParam, sLength, baBuffer );
        Value = GetUInt24( baBuffer );
        //Value = StringToUInt32( pszParam, strlen(pszParam) );
        SetUInt24(Value, baBuffer);
        HexArrayToString( baBuffer, sLength , oResult );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETUINT32", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL)
           return DIAG_RC_INVALID_PARAM;
      StringToHexArray( pszParam, strlen(pszParam), baBuffer);
      Value = GetUInt32(baBuffer);
      sprintf(oResult, "%08lx", Value);
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETUINT32", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( sLength != 8)
           return DIAG_RC_INVALID_PARAM;
        sLength = StringToHexArray( iParam, sLength, baBuffer );
        Value = GetUInt32( baBuffer );
        //Value = StringToUInt32( pszParam, strlen(pszParam) );
        SetUInt32(Value, baBuffer);
        HexArrayToString( baBuffer, sLength, oResult );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETUINT32LE", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL)
           return DIAG_RC_INVALID_PARAM;
      sLength = StringToHexArray( pszParam, strlen(pszParam), baBuffer);
      Value = GetUInt32_LittleEndian(baBuffer);
      sprintf(oResult, "%08lx", Value);
      //SetUInt32_LittleEndian(Value, temp);
      //HexArrayToString( temp, 4 , oResult );
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "SETUINT32LE", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( sLength != 8)
           return DIAG_RC_INVALID_PARAM;
        sLength = StringToHexArray( iParam, sLength, baBuffer );
        Value = GetUInt32( baBuffer );
        //Value = StringToUInt32( pszParam, strlen(pszParam) );
        SetUInt32_LittleEndian(Value, baBuffer);
        HexArrayToString( baBuffer, sLength, oResult );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "TOUPPER", iFuncName ) )
    {
        pszParam = strtok( iParam, "|" );
        if( pszParam == NULL)
           return DIAG_RC_INVALID_PARAM;
        
        ToUpperString( pszParam, baBuffer );
        strcpy( oResult, baBuffer );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "STRTOHEX", iFuncName ) )
    {
        Value16 = StringToHexArray(iParam, strlen(iParam), baBuffer2 );
        sLength = sprintf(oResult, "%u|", Value16);
        HexArrayToString( baBuffer2, Value16, &oResult[sLength]  );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "HEXTOSTRING", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL )
           return DIAG_RC_INVALID_PARAM;
      
      sLength = StringToHexArray(pszParam, strlen(pszParam), strReturn );
      HexArrayToString( strReturn, sLength, oResult );
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "STRTOUINT32", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL )
           return DIAG_RC_INVALID_PARAM;
      
      Value = StringToUInt32(pszParam, strlen(pszParam));
      sprintf(oResult, "%lu", Value);
      return DIAG_RC_SUCCESS;
    }
    else
        return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagLib_RecFile( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t isResult;
    UInt16_t Value16;
    UInt8_t baBuffer[1024];   
    Char_t *pszParam;
    
    if( 0 == strcmp( "CREATE", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL)
        return DIAG_RC_INVALID_PARAM;
      isResult = RecFile_CreateNew( pszParam, &hDiagRecFile);
      oResult[0] = isResult + '0';
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "OPEN", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL)
        return DIAG_RC_INVALID_PARAM;
      isResult = RecFile_Open( pszParam, &hDiagRecFile);
      oResult[0] = isResult + '0';
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "CLOSE", iFuncName ) )
    {
      RecFile_Close(&hDiagRecFile);
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "READ", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL)
        return DIAG_RC_INVALID_PARAM;
      Value16 = StringToUInt32( pszParam, strlen(pszParam));
      isResult = RecFile_ReadRecord(&hDiagRecFile, baBuffer, &Value16);
      oResult[0] = isResult + '0';
      oResult[1] = '|';
      HexArrayToString( baBuffer, Value16, &oResult[2] );
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "UPDATE", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL)
        return DIAG_RC_INVALID_PARAM; 
      Value16 = StringToHexArray( pszParam, strlen(pszParam), baBuffer );
      isResult = RecFile_UpdateRecord(&hDiagRecFile, baBuffer, Value16);
      oResult[0] = isResult + '0';
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "APPEND", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL)
        return DIAG_RC_INVALID_PARAM; 
      Value16 = StringToHexArray( pszParam, strlen(pszParam), baBuffer );
      isResult = RecFile_AppendRecord( &hDiagRecFile, baBuffer, Value16);
      oResult[0] = isResult + '0';
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GOTOFIRST", iFuncName ) )
    {
      RecFile_GotoFirstRecord( &hDiagRecFile );
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GOTONEXT", iFuncName ) )
    {
      RecFile_GotoFirstRecord( &hDiagRecFile );
      return DIAG_RC_SUCCESS;
    }
    else
        return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagLib_ImagePBM( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    UInt8_t     baBuffer[10000];
    UInt16_t    sWidth;
    UInt16_t    sHeight;
    UInt16_t    sLength;
    UInt16_t    sLength2;
    UInt16_t    sRetcode;
    
    if( 0 == strcmp( "GETBMP", iFuncName ) )
    {
        sRetcode = PBM_GetBitmap( iParam, &sWidth, &sHeight, baBuffer);
        sLength = sprintf( oResult, "%d|%d|%d|", sRetcode, sWidth, sHeight );
        sLength2 = (sHeight / 8);
        if( sHeight & 0x07 ) // if( 0 != ( sHeight % 8))
        {
                sLength2++;
        }
        sLength2 = sLength2 * sWidth;
        HexArrayToString( baBuffer, sLength2, &oResult[sLength] );
        return DIAG_RC_SUCCESS;
    }
    else
        return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagLib_Hash( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Char_t *pszParam;
    UInt8_t temp[1024];
    Bool_t isResult;
    
    if( 0 == strcmp( "RESET", iFuncName ) )
    {
      SHA1_Reset( &context );
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "INPUT", iFuncName ) )
    {
      pszParam = strtok( iParam, "|" );
      if( pszParam == NULL)
        return DIAG_RC_INVALID_PARAM;
      
      StringToHexArray( pszParam, strlen(pszParam), temp );
      isResult = SHA1_Input( &context, temp, strlen(temp) );
      oResult[0] = isResult + '0';
      return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "RESULT", iFuncName ) )
    {
      isResult = SHA1_Result(&context, temp );
      oResult[0] = isResult + '0';
      oResult[1] = '|';
      HexArrayToString( temp, strlen(temp), &oResult[2] );      /* Error might occur, if Hex Array contain 0 in the middle */
      return DIAG_RC_SUCCESS;
    }
    else
        return DIAG_RC_INVALID_CMD;
}

UInt8_t DiagLib_SamManager( Char_t iFuncName[], Char_t iParam[], Char_t oResult[] )
{
    Bool_t      isResult;
    Char_t      *pszParam1;
    Char_t      *pszParam2;
    Char_t      *pszParam3;
    UInt8_t     bValue;
    UInt8_t     bValue2;
    UInt8_t     bValue3;
    UInt8_t     baBuffer[1024];
    UInt8_t     baBuffer2[1024];
    UInt16_t    sLength;
    UInt16_t    sValue;
    UInt16_t    sValue2;
    
    if( 0 == strcmp( "INIT", iFuncName ) )
    {
        SAM_Init( );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "ACTIVATE", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        StringToHexArray( pszParam1, sLength, baBuffer );
        
        sLength = strlen( pszParam2 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue = ( UInt8_t )StringToUInt32( pszParam2, sLength );
        isResult = SAM_Activate( baBuffer, bValue, &bValue2 );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        sprintf( &oResult[2], "%d", bValue2 );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "DEACTIV", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue = ( UInt8_t )StringToUInt32( iParam, sLength );
        SAM_Deactivate( bValue );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "GETINFO", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue = ( UInt8_t )StringToUInt32( iParam, sLength );
        isResult = SAM_GetInfo( bValue, &bValue2, baBuffer, &bValue3 );
        sLength = sprintf( oResult, "%c|%d|", isResult + '0', bValue2 );
        HexArrayToString( baBuffer, bValue3, &oResult[sLength] );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "EXAPDU", iFuncName ) )
    {
        sLength = strlen( iParam );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue = ( UInt8_t )StringToUInt32( iParam, sLength );
        isResult = SAM_ExchangeAPDU( bValue, &hDiagAPDU );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        sprintf( &oResult[2], "%02x,%02x,%02x,%02x,%04x,%04x,%s,%04x", hDiagAPDU.m_CLA, \
                                                                       hDiagAPDU.m_INS, \
                                                                       hDiagAPDU.m_P1,  \
                                                                       hDiagAPDU.m_P2,  \
                                                                       hDiagAPDU.m_Lc,  \
                                                                       hDiagAPDU.m_Le,  \
                                                                       hDiagAPDU.m_Data,\
                                                                       hDiagAPDU.m_SW12 );
        return DIAG_RC_SUCCESS;
    }
    else if( 0 == strcmp( "EXRAW", iFuncName ) )
    {
        pszParam1 = strtok( iParam, "|" );
        pszParam2 = strtok( NULL, "|" );
        if( ( NULL == pszParam1 ) || ( NULL == pszParam2 ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        sLength = strlen( pszParam1 );
        if( ( 0 == sLength ) || ( 10 < sLength ) )
        {
            return DIAG_RC_INVALID_PARAM;
        }
        bValue = ( UInt8_t )StringToUInt32( pszParam1, sLength );
        sLength = strlen( pszParam2 );
        sValue = StringToHexArray( pszParam2, sLength, baBuffer );
        isResult = SAM_ExchangeRAW( bValue, baBuffer, sValue, baBuffer2, &sValue2 );
        oResult[0] = isResult + '0';
        oResult[1] = '|';
        HexArrayToString( baBuffer2, sValue2, &oResult[2] );
        return DIAG_RC_SUCCESS;
    }
    return DIAG_RC_INVALID_CMD;
}

/* Public Functions */
UInt8_t DiagLib_Execute( Char_t iCommand[], Char_t iParam[], Char_t oResult[] )
{
    Char_t      szFuncName[17]; /* up to 16 chars */
    UInt8_t     bStatus;

    memset( szFuncName, 0, sizeof(szFuncName) );
    memcpy( szFuncName, &iCommand[4], (strlen(iCommand) - 4) );    
    
    /* Compare module name */
    if( 0 == memcmp( "ATF", &iCommand[1], 3 ) ) /* Atomic File */
    {
        bStatus = DiagLib_AtomicFile( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "CRC", &iCommand[1], 3 ) ) /* Checksum */
    {
        bStatus = DiagLib_Checksum( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "CLM", &iCommand[1], 3 ) ) /* Ctls Manager */
    {
        bStatus = DiagLib_CrlsManager( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "DTM", &iCommand[1], 3 ) ) /* Date Time */
    {
        bStatus = DiagLib_DateTime( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "FLI", &iCommand[1], 3 ) ) /* File List */
    {
        bStatus = DiagLib_FileList( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "TLV", &iCommand[1], 3 ) ) /* TLV */
    {
        bStatus = DiagLib_TLV( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "RFL", &iCommand[1], 3 ) ) /* RecFile */
    {
        bStatus = DiagLib_RecFile( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "LIB", &iCommand[1], 3 ) ) /* Library */
    {
        bStatus = DiagLib_Library( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "PBM", &iCommand[1], 3 ) ) /* ImagePBM */
    {
        bStatus = DiagLib_ImagePBM( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "SHA", &iCommand[1], 3 ) ) /* Hash */
    {
        bStatus = DiagLib_Hash( szFuncName, iParam, oResult );
    }
    else if( 0 == memcmp( "SAM", &iCommand[1], 3 ) ) /* Sam Manager */
    {
        bStatus = DiagLib_SamManager( szFuncName, iParam, oResult );
    }
    else
    {
        bStatus = DIAG_RC_INVALID_CMD;
    }
    return bStatus;
}

/* EOF */
